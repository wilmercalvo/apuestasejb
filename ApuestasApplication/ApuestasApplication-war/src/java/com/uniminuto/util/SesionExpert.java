/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.util;

import com.uniminuto.entity.Apuesta;
import com.uniminuto.entity.Boletas;
import com.uniminuto.entity.Campeonato;
import com.uniminuto.entity.Deporte;
import com.uniminuto.entity.EquipoJugador;
import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Marcador;
import com.uniminuto.entity.Parametros;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.Perfil;
import com.uniminuto.entity.Premios;
import com.uniminuto.entity.Rifa;
import com.uniminuto.entity.Usuario;
import com.uniminuto.entity.VentaApuestas;
import com.uniminuto.entity.VentaRifas;
import com.uniminuto.session.ApuestaFacadeLocal;
import com.uniminuto.session.BoletasFacadeLocal;
import com.uniminuto.session.CampeonatoFacadeLocal;
import com.uniminuto.session.DeporteFacadeLocal;
import com.uniminuto.session.EquipoJugadorFacadeLocal;
import com.uniminuto.session.EstadosFacadeLocal;
import com.uniminuto.session.MarcadorFacadeLocal;
import com.uniminuto.session.ParametrosFacadeLocal;
import com.uniminuto.session.PartidoFacadeLocal;
import com.uniminuto.session.PerfilFacadeLocal;
import com.uniminuto.session.PremiosFacadeLocal;
import com.uniminuto.session.RifaFacadeLocal;
import com.uniminuto.session.UsuarioFacadeLocal;
import com.uniminuto.session.VentaApuestasFacadeLocal;
import com.uniminuto.session.VentaRifasFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author User
 */
public class SesionExpert {
    
    private static SesionExpert instance;
    
       
    public void redireccionarMensaje(HttpServletResponse response, String mensaje, String ruta)throws ServletException, IOException{
        PrintWriter out = response.getWriter();
        out.print("<script>if(confirm('"+mensaje+"')){window.location = '/ApuestasApplication-war/"+ruta+"'}else{window.location = '/ApuestasApplication-war/"+ruta+"'} </script>");
        out.close();
    }
    
    public void mensaje(HttpServletResponse response, String mensaje)throws ServletException, IOException{
        PrintWriter out = response.getWriter();
        out.print(mensaje);
        out.close();
    }
    
        public void redireccionar(HttpServletRequest request, HttpServletResponse response, String path)throws ServletException, IOException{
        RequestDispatcher rd = request.getRequestDispatcher("./"+path);
        rd.forward(request, response);
    }
    
    public void setSession(HttpServletRequest request,String clave, Object valor){
        HttpSession session = request.getSession();
        session.setAttribute(clave, valor);
    }
    
    public Object getSession(HttpServletRequest request,String clave){
        HttpSession session = request.getSession();
        return session.getAttribute(clave);
    }
    
    public void cerrarSession(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.invalidate();
    }
    
 public static SesionExpert getInstance(){
     if(instance != null){
         return instance;
     }else{
         return instance = new SesionExpert();
     }
 }   
}
