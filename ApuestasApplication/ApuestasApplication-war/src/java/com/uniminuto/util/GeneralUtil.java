/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.util;

/**
 *
 * @author User
 */
public class GeneralUtil {

private final static int UNMASKED_NUMBERS_LENGTH = 4;
private final static char MASK_CHAR = '*';
private static GeneralUtil instance;

   public String maskProductNumber(String productNumber) {
        if (productNumber == null || productNumber.length() < UNMASKED_NUMBERS_LENGTH) {
            return null;
        }
        int length = productNumber.length();
        int start = length - UNMASKED_NUMBERS_LENGTH;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < start; i++) {
            stringBuilder.append(MASK_CHAR);
        }
            stringBuilder.append(productNumber.substring(length - 4, length));
            productNumber = stringBuilder.toString();
            return productNumber;
        }
   
   public static GeneralUtil getInstance(){
       if(instance != null){
           return instance;
       }else{
           return instance = new GeneralUtil();
       }
   }
    
}

