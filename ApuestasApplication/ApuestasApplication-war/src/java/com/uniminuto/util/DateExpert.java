/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author User
 */
public class DateExpert {   
   
    private static DateExpert instance; 
    
    private static final SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    
    
    public String fechaString(Date fechaS){
        return fecha.format(fechaS);
    }
    
    public Date fechaDate(String fechaD){
        try{
            return fecha.parse(fechaD);
        }catch(Exception ex){
            return null;
        }
    }
    
    public int diferenciaFecha(Date fechaAnalizar){
        Date fechaActual = new Timestamp(System.currentTimeMillis());
        long diferenciaen_ms =  fechaAnalizar.getTime() - fechaActual.getTime();
        long dias = diferenciaen_ms / (1000 * 60);
        return (int) dias;
    }
    
    public int diferenciaDosFecha(Date fechaMayor, Date fechaMenor){
        long diferenciaen_ms =  fechaMayor.getTime() - fechaMenor.getTime();
        long dias = diferenciaen_ms / (1000 * 60);
        return (int) dias;
    }
    
    public int diferenciaFechaDias(Date fechaAnalizar){
        Date fechaActual = new Timestamp(System.currentTimeMillis());
        long diferenciaen_ms =  fechaAnalizar.getTime() - fechaActual.getTime();
        long dias = diferenciaen_ms / (1000 * 60 * 60 * 24);
        return (int) dias;
    }
    
    public static DateExpert getInstance(){
        if(instance != null){
            return instance;
        }else{
            return instance = new DateExpert();
        }
    }
}
