/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.util;

import com.uniminuto.entity.Apuesta;
import com.uniminuto.entity.Boletas;
import com.uniminuto.entity.Campeonato;
import com.uniminuto.entity.Deporte;
import com.uniminuto.entity.EquipoJugador;
import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Marcador;
import com.uniminuto.entity.Parametros;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.Perfil;
import com.uniminuto.entity.Premios;
import com.uniminuto.entity.Rifa;
import com.uniminuto.entity.Usuario;
import com.uniminuto.entity.VentaApuestas;
import com.uniminuto.entity.VentaRifas;
import java.util.List;

/**
 *
 * @author User
 */
public class UtilDTO {
    
    private List<Apuesta> apuestas;
    private List<Boletas> boletas;
    private List<Campeonato> campeonatos;
    private List<Deporte> deportes;
    private List<EquipoJugador> equipoJugadores;
    private List<Estados> estados;
    private List<Parametros> parametros;
    private List<Perfil> perfiles;
    private List<Premios> premios;
    private List<Rifa> rifas;
    private List<VentaApuestas> ventaApuestas;
    private List<VentaRifas> ventaRifas;
    private List<Usuario> usuarios;
    private List<Partido> partidos;
    private List<Marcador> marcadores;

    public UtilDTO() {
    }

    public List<Apuesta> getApuestas() {
        return apuestas;
    }

    public void setApuestas(List<Apuesta> apuestas) {
        this.apuestas = apuestas;
    }

    public List<Boletas> getBoletas() {
        return boletas;
    }

    public void setBoletas(List<Boletas> boletas) {
        this.boletas = boletas;
    }

    public List<Campeonato> getCampeonatos() {
        return campeonatos;
    }

    public void setCampeonatos(List<Campeonato> campeonatos) {
        this.campeonatos = campeonatos;
    }

    public List<Deporte> getDeportes() {
        return deportes;
    }

    public void setDeportes(List<Deporte> deportes) {
        this.deportes = deportes;
    }

    public List<EquipoJugador> getEquipoJugadores() {
        return equipoJugadores;
    }

    public void setEquipoJugadores(List<EquipoJugador> equipoJugadores) {
        this.equipoJugadores = equipoJugadores;
    }

    public List<Estados> getEstados() {
        return estados;
    }

    public void setEstados(List<Estados> estados) {
        this.estados = estados;
    }

    public List<Parametros> getParametros() {
        return parametros;
    }

    public void setParametros(List<Parametros> parametros) {
        this.parametros = parametros;
    }

    public List<Perfil> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public List<Premios> getPremios() {
        return premios;
    }

    public void setPremios(List<Premios> premios) {
        this.premios = premios;
    }

    public List<Rifa> getRifas() {
        return rifas;
    }

    public void setRifas(List<Rifa> rifas) {
        this.rifas = rifas;
    }

    public List<VentaApuestas> getVentaApuestas() {
        return ventaApuestas;
    }

    public void setVentaApuestas(List<VentaApuestas> ventaApuestas) {
        this.ventaApuestas = ventaApuestas;
    }

    public List<VentaRifas> getVentaRifas() {
        return ventaRifas;
    }

    public void setVentaRifas(List<VentaRifas> ventaRifas) {
        this.ventaRifas = ventaRifas;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Partido> getPartidos() {
        return partidos;
    }

    public void setPartidos(List<Partido> partidos) {
        this.partidos = partidos;
    }

    public List<Marcador> getMarcadores() {
        return marcadores;
    }

    public void setMarcadores(List<Marcador> marcadores) {
        this.marcadores = marcadores;
    }
    
    
}
