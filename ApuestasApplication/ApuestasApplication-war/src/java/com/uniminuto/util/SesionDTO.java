/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.util;

/**
 *
 * @author User
 */
public class SesionDTO {
    private int idUsuario;
    private String userName;
    private int perfil;
    private int cupo;
    private String nombre;
    private String redirecionPagina;

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getPerfil() {
        return perfil;
    }

    public void setPerfil(int perfil) {
        this.perfil = perfil;
    }

    public int getCupo() {
        return cupo;
    }

    public void setCupo(int cupo) {
        this.cupo = cupo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRedirecionPagina() {
        return redirecionPagina;
    }

    public void setRedirecionPagina(String redirecionPagina) {
        this.redirecionPagina = redirecionPagina;
    }
    
    
}
