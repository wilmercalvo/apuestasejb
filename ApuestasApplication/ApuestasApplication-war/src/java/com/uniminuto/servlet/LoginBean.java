/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.servlet;


import com.uniminuto.entity.Apuesta;
import com.uniminuto.entity.Boletas;
import com.uniminuto.entity.Campeonato;
import com.uniminuto.entity.Deporte;
import com.uniminuto.entity.EquipoJugador;
import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Marcador;
import com.uniminuto.entity.Parametros;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.Perfil;
import com.uniminuto.entity.Premios;
import com.uniminuto.entity.Rifa;
import com.uniminuto.entity.Usuario;
import com.uniminuto.entity.VentaApuestas;
import com.uniminuto.entity.VentaRifas;
import com.uniminuto.session.ApuestaFacadeLocal;
import com.uniminuto.session.BoletasFacadeLocal;
import com.uniminuto.session.CampeonatoFacadeLocal;
import com.uniminuto.session.DeporteFacadeLocal;
import com.uniminuto.session.EquipoJugadorFacadeLocal;
import com.uniminuto.session.EstadosFacadeLocal;
import com.uniminuto.session.MarcadorFacadeLocal;
import com.uniminuto.session.ParametrosFacadeLocal;
import com.uniminuto.session.PartidoFacadeLocal;
import com.uniminuto.session.PerfilFacadeLocal;
import com.uniminuto.session.PremiosFacadeLocal;
import com.uniminuto.session.RifaFacadeLocal;
import com.uniminuto.session.UsuarioFacadeLocal;
import com.uniminuto.session.VentaApuestasFacadeLocal;
import com.uniminuto.session.VentaRifasFacadeLocal;
import com.uniminuto.util.SesionDTO;
import com.uniminuto.util.SesionExpert;
import com.uniminuto.util.UtilDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author User
 */
public class LoginBean extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
        @EJB
        ApuestaFacadeLocal apuestaE;
        @EJB
        BoletasFacadeLocal boletasE;
        @EJB
        CampeonatoFacadeLocal campeonatosE;
        @EJB
        DeporteFacadeLocal deportesE;
        @EJB
        EquipoJugadorFacadeLocal equipoJugadoresE;
        @EJB
        EstadosFacadeLocal estadosE;
        @EJB
        ParametrosFacadeLocal parametrosE;
        @EJB
        PerfilFacadeLocal perfilesE;
        @EJB
        PremiosFacadeLocal premiosE;
        @EJB
        RifaFacadeLocal rifasE;
        @EJB
        VentaApuestasFacadeLocal ventaApuestasE;
        @EJB
        VentaRifasFacadeLocal ventaRifasE;
        @EJB
        UsuarioFacadeLocal usuariosE;
        @EJB
        PartidoFacadeLocal partidosE;
        @EJB
        MarcadorFacadeLocal marcadoresE;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginBean</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginBean at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String user = request.getParameter("user");
        String pass = request.getParameter("pass");
        List<Usuario> object = usuariosE.selectQueryLogin("Usuario.login",user,pass);
        
        if(object.size() > 0){
            SesionExpert out = new SesionExpert();
            for (Usuario usuario : object) {
                if(1 == usuario.getIdEstado().getId()){
                    if(1 == usuario.getIdPerfil().getId()){
                        SesionExpert.getInstance().setSession(request, "idUsuario",usuario.getId());
                        SesionExpert.getInstance().setSession(request, "userName",usuario.getUsuario());
                        SesionExpert.getInstance().setSession(request, "perfil",usuario.getIdPerfil().getId());
                        SesionExpert.getInstance().setSession(request, "nombre",usuario.getNombre().toUpperCase());
                        SesionExpert.getInstance().setSession(request, "redirecionPagina","HomeAdmin.jsp");
                        SesionExpert.getInstance().setSession(request, "listas",cargarObjetos());
                        out.redireccionar(request,response,"HomeAdmin.jsp");
                    }else if(2 == usuario.getIdPerfil().getId()){
                        SesionExpert.getInstance().setSession(request, "idUsuario",usuario.getId());
                        SesionExpert.getInstance().setSession(request, "userName",usuario.getUsuario());
                        SesionExpert.getInstance().setSession(request, "perfil",usuario.getIdPerfil().getId());
                        SesionExpert.getInstance().setSession(request, "nombre",usuario.getNombre().toUpperCase());
                        SesionExpert.getInstance().setSession(request, "redirecionPagina","HomeVentas.jsp");
                        SesionExpert.getInstance().setSession(request, "listas",cargarObjetos());
                        out.redireccionar(request,response,"HomeVentas.jsp");
                    }else if(3 == usuario.getIdPerfil().getId()){
                        SesionExpert.getInstance().setSession(request, "idUsuario",usuario.getId());
                        SesionExpert.getInstance().setSession(request, "userName",usuario.getUsuario());
                        SesionExpert.getInstance().setSession(request, "perfil",usuario.getIdPerfil().getId());
                        SesionExpert.getInstance().setSession(request, "cupo",usuario.getCupoDinero());
                        SesionExpert.getInstance().setSession(request, "nombre",usuario.getNombre().toUpperCase());
                        SesionExpert.getInstance().setSession(request, "redirecionPagina","HomeUsuario.jsp");
                        SesionExpert.getInstance().setSession(request, "listas",cargarObjetos());
                        SesionDTO sesioDTO = new SesionDTO();
                        sesioDTO.setIdUsuario(usuario.getId());
                        sesioDTO.setUserName(usuario.getUsuario());
                        sesioDTO.setPerfil(usuario.getIdPerfil().getId());
                        sesioDTO.setCupo(usuario.getCupoDinero());
                        sesioDTO.setNombre(usuario.getNombre().toUpperCase());
                        sesioDTO.setRedirecionPagina("HomeUsuario.jsp");
                        SesionExpert.getInstance().setSession(request, "sesionDTO",sesioDTO);
                        out.redireccionar(request,response,"HomeUsuario.jsp");
                    }else{
                        out.redireccionarMensaje(response, "El usuario " + usuario.getNombre() + " NO tiene perfil definido", "index.jsp");
                    }
                }else{
                    out.redireccionarMensaje(response, "El usuario " + usuario.getNombre() + " NO se encuentra con estado ACTIVO", "index.jsp");
                }
            }
        }else{
            SesionExpert out = new SesionExpert();
            out.redireccionarMensaje(response, "Usuario NO se encuentra registrado", "index.jsp");
        }
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public UtilDTO cargarObjetos(){
        
       List<Apuesta> apuestas = apuestaE.findAll();
       List<Boletas> boletas = boletasE.findAll();
       List<Campeonato> campeonatos = campeonatosE.findAll();
       List<Deporte> deportes = deportesE.findAll();
       List<EquipoJugador> equipoJugadores = equipoJugadoresE.findAll();
       List<Estados> estados = estadosE.findAll();
       List<Parametros> parametros = parametrosE.findAll();
       List<Perfil> perfiles = perfilesE.findAll();
       List<Premios> premios = premiosE.findAll();
       List<Rifa> rifas = rifasE.findAll();
       List<VentaApuestas> ventaApuestas = ventaApuestasE.findAll();
       List<VentaRifas> ventaRifas = ventaRifasE.findAll();
       List<Usuario> usuarios = usuariosE.findAll();
       List<Partido> partidos =  partidosE.findAll();
       List<Marcador> marcadores = marcadoresE.findAll();
       UtilDTO inDTO = new UtilDTO();
       inDTO.setApuestas(apuestas);
       inDTO.setBoletas(boletas);
       inDTO.setCampeonatos(campeonatos);
       inDTO.setDeportes(deportes);
       inDTO.setEquipoJugadores(equipoJugadores);
       inDTO.setEstados(estados);
       inDTO.setParametros(parametros);
       inDTO.setPerfiles(perfiles);
       inDTO.setPremios(premios);
       inDTO.setRifas(rifas);
       inDTO.setVentaApuestas(ventaApuestas);
       inDTO.setVentaRifas(ventaRifas);
       inDTO.setUsuarios(usuarios);
       inDTO.setPartidos(partidos);
       inDTO.setMarcadores(marcadores);
       return inDTO;
    }
    
}
