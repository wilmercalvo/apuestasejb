/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.servlet;

import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Perfil;
import com.uniminuto.entity.Usuario;
import com.uniminuto.session.UsuarioFacadeLocal;
import com.uniminuto.util.SesionExpert;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author User
 */
public class AdminRegistrarUsuarioBean extends HttpServlet {

    @EJB
    private UsuarioFacadeLocal usuariosE;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminRegistrarUsuarioBean</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminRegistrarUsuarioBean at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        Usuario user = new Usuario();
        user.setNombre(request.getParameter("nombre"));
        user.setUsuario(request.getParameter("username"));
        user.setPassword(request.getParameter("password"));
        Perfil perfil = new Perfil();
        perfil.setId(3);
        user.setIdPerfil(perfil);
        user.setNumeroDocumento(request.getParameter("numeroD"));
        user.setCupoDinero(0);
        Estados estados = new Estados();
        estados.setId(1);
        user.setIdEstado(estados);
        usuariosE.create(user);
        //SesionExpert.getInstance().setSession(request, "listas",SesionExpert.getInstance().cargarObjetos());
        SesionExpert.getInstance().redireccionarMensaje(response, "Registro exitoso", "index.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
