/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.servlet;


import com.uniminuto.entity.Apuesta;
import com.uniminuto.entity.Marcador;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.Rifa;
import com.uniminuto.session.ApuestaFacadeLocal;
import com.uniminuto.session.MarcadorFacadeLocal;
import com.uniminuto.session.PartidoFacadeLocal;
import com.uniminuto.session.RifaFacadeLocal;
import com.uniminuto.util.DateExpert;
import com.uniminuto.util.SesionExpert;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author User
 */
public class CatalogoBean extends HttpServlet {

    @EJB
    private ApuestaFacadeLocal apuestaE;
    @EJB
    private RifaFacadeLocal rifasE;
    @EJB
    private PartidoFacadeLocal partidosE;
    @EJB
    private MarcadorFacadeLocal marcadoresE;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    int numApuesta = 0;
    int numRifa = 0;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CatalogoBean</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CatalogoBean at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String datosCatalogo = "{\"type\": 1, \"responseApuesta\": " + apuestas() + ", \"tamApuesta\": " + this.numApuesta + ", \"resultados\": " + resultadosApuesta()+ ", \"responseRifa\": " + Rifas()+ ", \"resultadosRifas\": " + resultadosRifas()+ ", \"tamRifa\": " + this.numRifa+ " }";
        this.numApuesta = 0;
        this.numRifa = 0;
        System.out.println("Datos Catalogo " + datosCatalogo);
        SesionExpert.getInstance().mensaje(response,datosCatalogo);
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String apuestas(){
            List<Apuesta> listaApuesta = apuestaE.findAll();
            JSONArray data = new JSONArray();
            for (Apuesta ventaApuestas : listaApuesta) {
                if(ventaApuestas.getIdPartido().getIdEstado().getId() == 1){ //consulta con estado activo apuestas
                    this.numApuesta ++;
                    JSONObject result = new JSONObject();
                         result.put("nombre", ventaApuestas.getNombre());
                         result.put("descripcion", ventaApuestas.getDescripcion());
                         result.put("equiLocal", ventaApuestas.getIdPartido().getIdEquipoLocal().getNombre());
                         result.put("equiVisit",ventaApuestas.getIdPartido().getIdEquipoVisitante().getNombre());
                         result.put("valor", ventaApuestas.getValor().toString());
                         result.put("fecha", ventaApuestas.getIdPartido().getFechaPartido());
                         data.add(result);
            }
        }
        return data.toString();    
    }
    public String resultadosApuesta(){
        JSONArray dataResultado = new JSONArray();
        List<Partido> partidos = partidosE.findAll();
        for (Partido partido : partidos) {
            if(partido.getIdEstado().getId() == 3 && DateExpert.getInstance().diferenciaFechaDias(DateExpert.getInstance().fechaDate(partido.getFechaPartido())) == 0){
                
                List<Marcador> marcadores = marcadoresE.selectQueryId("Marcador.findById", partido.getMarcadorJuego().toString());
                try{
                    JSONObject result = new JSONObject();
                    result.put("resultado",partido.getIdEquipoLocal().getNombre() +" vs "+partido.getIdEquipoVisitante().getNombre() +"  ("+marcadores.get(0).getValor()+") ");
                    dataResultado.add(result);
                }catch(Throwable ex){
                   System.out.println("se presento el error [ "+ex.getMessage()+" ] en el metodo ");
                }
            }
        }
        return dataResultado.toString();
    }
    
    public String Rifas(){
        JSONArray dataResultado = new JSONArray();
        List<Rifa> rifa = rifasE.findAll();
        for (Rifa rifa1 : rifa) {
            if(rifa1.getIdEstado().getId() == 1  && (rifa1.getCuposParticipantes() - rifa1.getCuposActualParticipantes()) > 0){
                try{
                    this.numRifa ++;
                    JSONObject result = new JSONObject();
                    result.put("nombre",rifa1.getNombre());
                    result.put("descripcion",rifa1.getDescripcion());
                    result.put("valor",rifa1.getValor());
                    result.put("fecha",rifa1.getFechaRifa());
                    result.put("p1",rifa1.getPremioPrincipal().getDescripcion());
                    result.put("p2",rifa1.getIdPremioSecundario1().getDescripcion());
                    result.put("p3",rifa1.getIdPremioSecundario2().getDescripcion());
                    result.put("p4",rifa1.getIdPremioSecundario3().getDescripcion());
                    result.put("p5",rifa1.getIdPremioSecundario4().getDescripcion());
                    result.put("cupos",(rifa1.getCuposParticipantes() - rifa1.getCuposActualParticipantes()));
                    dataResultado.add(result);
                }catch(Throwable ex){
                   System.out.println("se presento el error [ "+ex.getMessage()+" ] en el metodo ");
                }
            }
        }
        return dataResultado.toString();
    }
    
    public String resultadosRifas(){
        JSONArray dataResultado = new JSONArray();
        List<Rifa> rifa = rifasE.findAll();
        ArrayList<Rifa> listaRifa = new ArrayList();
        for (Rifa rifa1 : rifa) {
            if(rifa1.getIdEstado().getId() == 3 && DateExpert.getInstance().diferenciaFechaDias(DateExpert.getInstance().fechaDate(rifa1.getFechaRifa())) == 0){
                try{
                    JSONObject result = new JSONObject();
                    result.put("resultado",rifa1.getNombre() +"  ("+rifa1.getResultado()+") ");
                    dataResultado.add(result);
                }catch(Throwable ex){
                   System.out.println("se presento el error [ "+ex.getMessage()+" ] en el metodo ");
                }
            }
        }
        return dataResultado.toString();
    }
}
