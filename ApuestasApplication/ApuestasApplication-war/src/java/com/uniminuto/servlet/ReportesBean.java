/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.servlet;

import com.uniminuto.entity.Marcador;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.VentaApuestas;
import com.uniminuto.entity.VentaRifas;
import com.uniminuto.session.MarcadorFacadeLocal;
import com.uniminuto.session.VentaApuestasFacadeLocal;
import com.uniminuto.session.VentaRifasFacadeLocal;
import com.uniminuto.util.DateExpert;
import com.uniminuto.util.GeneralUtil;
import com.uniminuto.util.SesionExpert;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author User
 */
public class ReportesBean extends HttpServlet {

    @EJB
    private VentaApuestasFacadeLocal ventaApuestasE;
    @EJB
    private VentaRifasFacadeLocal ventaRifasE;
    @EJB
    private MarcadorFacadeLocal marcadoresE;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ReportesBean</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ReportesBean at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
                    String flag = request.getParameter("type");
                    if(flag.equals("1")){//rifa
                        String datosGanadores = "{\"reporte\": " + rifa()+ " }";
                        System.out.println("reporte " + datosGanadores);
                        SesionExpert.getInstance().mensaje(response,datosGanadores);
                    }else if(flag.equals("2")){ //apuesta
                        System.out.println(" deporte: " + request.getParameter("deporte") +" campe: "+request.getParameter("campeonato") +" fecha: "+request.getParameter("fecha"));
                        int deporte = 0;
                        int campeonato =0;
                        String fecha = null;
                        if(!request.getParameter("deporte").equals("")){
                            System.out.println("no null deporte");
                            deporte = Integer.parseInt(request.getParameter("deporte"));
                        }
                        if(!request.getParameter("campeonato").equals("")){
                            campeonato = Integer.parseInt(request.getParameter("campeonato"));
                        }
                        if(!request.getParameter("fecha").equals("")){
                            fecha = request.getParameter("fecha");
                        }
                        
                        System.out.println(" deporte: " + deporte +" campe: "+campeonato +" fecha: "+fecha);
                        String datosGanadores = "{\"reporte\": " + apuesta(deporte, campeonato, fecha)+ " }";
                        System.out.println("reporte " + datosGanadores);
                        SesionExpert.getInstance().mensaje(response,datosGanadores);
                    }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String rifa(){
        System.out.println("Ingresa Reporte Rifa");
        JSONArray data = new JSONArray();
        List<VentaRifas> rifaV = ventaRifasE.findAll();
        for (VentaRifas rifa1 : rifaV) {
                try{
                    JSONObject result = new JSONObject();
                    result.put("nombre",rifa1.getIdUsuario().getNombre());
                    result.put("evento",rifa1.getIdRifa().getNombre());
                    result.put("cc",GeneralUtil.getInstance().maskProductNumber(rifa1.getIdUsuario().getNumeroDocumento()));
                    result.put("resultado",rifa1.getResultado());
                    result.put("valorApostado",rifa1.getMarcadorJuego());
                    result.put("fecha",rifa1.getIdRifa().getFechaRifa());
                    result.put("valor",rifa1.getIdRifa().getValor());
                    result.put("resultadoEvento",rifa1.getIdRifa().getResultado());
                    result.put("estado",rifa1.getIdEstado().getDescripcion());
                    data.add(result);
                }catch(Throwable ex){
                   System.out.println("se presento el error [ "+ex.getMessage()+" ] en el metodo ");
                }
        }
        return data.toString();
    }
    
    public String apuesta(int deporte, int campeonato, String fecha){
        System.out.println("Ingresa reporte apuesta");
        JSONArray data = new JSONArray();
        List<VentaApuestas> apuestaV = ventaApuestasE.findAll();
        for(VentaApuestas apuesta1 : apuestaV) {
                try{
                    Partido pp = apuesta1.getIdApuesta().getIdPartido();
                    if(deporte > 0 && campeonato > 0 && fecha != null){
                        System.out.println("1 2 3");
                        if(pp.getIdDeporte().getId() == deporte && (pp.getIdEquipoLocal().getIdCampeonato().getId() == campeonato || pp.getIdEquipoVisitante().getIdCampeonato().getId() == campeonato) &&
                                 DateExpert.getInstance().diferenciaDosFecha(DateExpert.getInstance().fechaDate(fecha), DateExpert.getInstance().fechaDate(pp.getFechaPartido())) >= 0){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 result.put("resultado",apuesta1.getResultado());
                                 result.put("valorApostado",apuesta1.getMarcadorJuego().getValor());
                                 result.put("fecha",apuesta1.getIdApuesta().getIdPartido().getFechaPartido());
                                 result.put("valor",apuesta1.getIdApuesta().getValor());
                                 List<Marcador> marcadores = marcadoresE.selectQueryId("Marcador.findById", apuesta1.getIdApuesta().getIdPartido().getMarcadorJuego().toString());
                                 if(marcadores.size() > 0){
                                     result.put("resultadoEvento",marcadores.get(0).getValor());
                                 }else{
                                     result.put("resultadoEvento","Pendiente");
                                 }
                                 result.put("estado",apuesta1.getIdEstado().getDescripcion());
                                 data.add(result);
                        }
                        
                    }else if(campeonato > 0 && fecha != null){
                        System.out.println("2 3");
                        if((pp.getIdEquipoLocal().getIdCampeonato().getId() == campeonato || pp.getIdEquipoVisitante().getIdCampeonato().getId() == campeonato) &&
                                 DateExpert.getInstance().diferenciaDosFecha(DateExpert.getInstance().fechaDate(fecha), DateExpert.getInstance().fechaDate(pp.getFechaPartido())) >= 0){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 result.put("resultado",apuesta1.getResultado());
                                 result.put("valorApostado",apuesta1.getMarcadorJuego().getValor());
                                 result.put("fecha",apuesta1.getIdApuesta().getIdPartido().getFechaPartido());
                                 result.put("valor",apuesta1.getIdApuesta().getValor());
                                 List<Marcador> marcadores = marcadoresE.selectQueryId("Marcador.findById", apuesta1.getIdApuesta().getIdPartido().getMarcadorJuego().toString());
                                 if(marcadores.size() > 0){
                                     result.put("resultadoEvento",marcadores.get(0).getValor());
                                 }else{
                                     result.put("resultadoEvento","Pendiente");
                                 }
                                 result.put("estado",apuesta1.getIdEstado().getDescripcion());
                                 data.add(result);
                        }
                        
                    }else if(deporte > 0 && fecha != null){
                        System.out.println("1 3");
                        if(pp.getIdDeporte().getId() == deporte && 
                                 DateExpert.getInstance().diferenciaDosFecha(DateExpert.getInstance().fechaDate(fecha), DateExpert.getInstance().fechaDate(pp.getFechaPartido())) >= 0){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 result.put("resultado",apuesta1.getResultado());
                                 result.put("valorApostado",apuesta1.getMarcadorJuego().getValor());
                                 result.put("fecha",apuesta1.getIdApuesta().getIdPartido().getFechaPartido());
                                 result.put("valor",apuesta1.getIdApuesta().getValor());
                                 List<Marcador> marcadores = marcadoresE.selectQueryId("Marcador.findById", apuesta1.getIdApuesta().getIdPartido().getMarcadorJuego().toString());
                                 if(marcadores.size() > 0){
                                     result.put("resultadoEvento",marcadores.get(0).getValor());
                                 }else{
                                     result.put("resultadoEvento","Pendiente");
                                 }
                                 result.put("estado",apuesta1.getIdEstado().getDescripcion());
                                 data.add(result);
                        }
                    }else if(deporte > 0 && campeonato > 0){
                        System.out.println("1 2");
                        if(pp.getIdDeporte().getId() == deporte && 
                                (pp.getIdEquipoLocal().getIdCampeonato().getId() == campeonato || pp.getIdEquipoVisitante().getIdCampeonato().getId() == campeonato)){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 result.put("resultado",apuesta1.getResultado());
                                 result.put("valorApostado",apuesta1.getMarcadorJuego().getValor());
                                 result.put("fecha",apuesta1.getIdApuesta().getIdPartido().getFechaPartido());
                                 result.put("valor",apuesta1.getIdApuesta().getValor());
                                 List<Marcador> marcadores = marcadoresE.selectQueryId("Marcador.findById", apuesta1.getIdApuesta().getIdPartido().getMarcadorJuego().toString());
                                 if(marcadores.size() > 0){
                                     result.put("resultadoEvento",marcadores.get(0).getValor());
                                 }else{
                                     result.put("resultadoEvento","Pendiente");
                                 }
                                 result.put("estado",apuesta1.getIdEstado().getDescripcion());
                                 data.add(result);
                        }
                    }else if(deporte  > 0){
                        System.out.println("1");
                        if(pp.getIdDeporte().getId() == deporte){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 result.put("resultado",apuesta1.getResultado());
                                 result.put("valorApostado",apuesta1.getMarcadorJuego().getValor());
                                 result.put("fecha",apuesta1.getIdApuesta().getIdPartido().getFechaPartido());
                                 result.put("valor",apuesta1.getIdApuesta().getValor());
                                 List<Marcador> marcadores = marcadoresE.selectQueryId("Marcador.findById", apuesta1.getIdApuesta().getIdPartido().getMarcadorJuego().toString());
                                 if(marcadores.size() > 0){
                                     result.put("resultadoEvento",marcadores.get(0).getValor());
                                 }else{
                                     result.put("resultadoEvento","Pendiente");
                                 }
                                 result.put("estado",apuesta1.getIdEstado().getDescripcion());
                                 data.add(result);
                        }
                    }else if(campeonato  > 0){
                        System.out.println("2");
                        if((pp.getIdEquipoLocal().getIdCampeonato().getId() == campeonato || pp.getIdEquipoVisitante().getIdCampeonato().getId() == campeonato)){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 result.put("resultado",apuesta1.getResultado());
                                 result.put("valorApostado",apuesta1.getMarcadorJuego().getValor());
                                 result.put("fecha",apuesta1.getIdApuesta().getIdPartido().getFechaPartido());
                                 result.put("valor",apuesta1.getIdApuesta().getValor());
                                 List<Marcador> marcadores = marcadoresE.selectQueryId("Marcador.findById", apuesta1.getIdApuesta().getIdPartido().getMarcadorJuego().toString());
                                 if(marcadores.size() > 0){
                                     result.put("resultadoEvento",marcadores.get(0).getValor());
                                 }else{
                                     result.put("resultadoEvento","Pendiente");
                                 }
                                 result.put("estado",apuesta1.getIdEstado().getDescripcion());
                                 data.add(result);
                        }
                    }else if(fecha != null){
                        System.out.println("3");
                        if(DateExpert.getInstance().diferenciaDosFecha(DateExpert.getInstance().fechaDate(fecha), DateExpert.getInstance().fechaDate(pp.getFechaPartido())) >= 0){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 result.put("resultado",apuesta1.getResultado());
                                 result.put("valorApostado",apuesta1.getMarcadorJuego().getValor());
                                 result.put("fecha",apuesta1.getIdApuesta().getIdPartido().getFechaPartido());
                                 result.put("valor",apuesta1.getIdApuesta().getValor());
                                 List<Marcador> marcadores = marcadoresE.selectQueryId("Marcador.findById", apuesta1.getIdApuesta().getIdPartido().getMarcadorJuego().toString());
                                 if(marcadores.size() > 0){
                                     result.put("resultadoEvento",marcadores.get(0).getValor());
                                 }else{
                                     result.put("resultadoEvento","Pendiente");
                                 }
                                 result.put("estado",apuesta1.getIdEstado().getDescripcion());
                                 data.add(result);
                        }
                    }else{
                            System.out.println("default");
                                JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 result.put("resultado",apuesta1.getResultado());
                                 result.put("valorApostado",apuesta1.getMarcadorJuego().getValor());
                                 result.put("fecha",apuesta1.getIdApuesta().getIdPartido().getFechaPartido());
                                 result.put("valor",apuesta1.getIdApuesta().getValor());
                                 List<Marcador> marcadores = marcadoresE.selectQueryId("Marcador.findById", apuesta1.getIdApuesta().getIdPartido().getMarcadorJuego().toString());
                                 if(marcadores.size() > 0){
                                     result.put("resultadoEvento",marcadores.get(0).getValor());
                                 }else{
                                     result.put("resultadoEvento","Pendiente");
                                 }
                                 
                                 result.put("estado",apuesta1.getIdEstado().getDescripcion());
                                 data.add(result);
                        }
                }catch(Throwable ex){
                   System.out.println("se presento el error [ "+ex.getMessage()+" ] en el metodo ");
                }
            }
        return data.toString();
    }
}
