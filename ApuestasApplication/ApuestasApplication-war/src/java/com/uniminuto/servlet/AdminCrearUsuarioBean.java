/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.servlet;

import com.uniminuto.entity.Apuesta;
import com.uniminuto.entity.Boletas;
import com.uniminuto.entity.Campeonato;
import com.uniminuto.entity.Deporte;
import com.uniminuto.entity.EquipoJugador;
import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Marcador;
import com.uniminuto.entity.Parametros;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.Perfil;
import com.uniminuto.entity.Premios;
import com.uniminuto.entity.Rifa;
import com.uniminuto.entity.Usuario;
import com.uniminuto.entity.VentaApuestas;
import com.uniminuto.entity.VentaRifas;
import com.uniminuto.session.ApuestaFacadeLocal;
import com.uniminuto.session.BoletasFacadeLocal;
import com.uniminuto.session.CampeonatoFacadeLocal;
import com.uniminuto.session.DeporteFacadeLocal;
import com.uniminuto.session.EquipoJugadorFacadeLocal;
import com.uniminuto.session.EstadosFacadeLocal;
import com.uniminuto.session.MarcadorFacadeLocal;
import com.uniminuto.session.ParametrosFacadeLocal;
import com.uniminuto.session.PartidoFacadeLocal;
import com.uniminuto.session.PerfilFacadeLocal;
import com.uniminuto.session.PremiosFacadeLocal;
import com.uniminuto.session.RifaFacadeLocal;
import com.uniminuto.session.UsuarioFacadeLocal;
import com.uniminuto.session.VentaApuestasFacadeLocal;
import com.uniminuto.session.VentaRifasFacadeLocal;
import com.uniminuto.util.SesionExpert;
import com.uniminuto.util.UtilDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author User
 */
public class AdminCrearUsuarioBean extends HttpServlet {

        @EJB
        ApuestaFacadeLocal apuestaE;
        @EJB
        BoletasFacadeLocal boletasE;
        @EJB
        CampeonatoFacadeLocal campeonatosE;
        @EJB
        DeporteFacadeLocal deportesE;
        @EJB
        EquipoJugadorFacadeLocal equipoJugadoresE;
        @EJB
        EstadosFacadeLocal estadosE;
        @EJB
        ParametrosFacadeLocal parametrosE;
        @EJB
        PerfilFacadeLocal perfilesE;
        @EJB
        PremiosFacadeLocal premiosE;
        @EJB
        RifaFacadeLocal rifasE;
        @EJB
        VentaApuestasFacadeLocal ventaApuestasE;
        @EJB
        VentaRifasFacadeLocal ventaRifasE;
        @EJB
        UsuarioFacadeLocal usuariosE;
        @EJB
        PartidoFacadeLocal partidosE;
        @EJB
        MarcadorFacadeLocal marcadoresE;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminCrearUsuarioBean</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminCrearUsuarioBean at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        Usuario user = new Usuario();
        user.setNombre(request.getParameter("nombre"));
        user.setUsuario(request.getParameter("user"));
        user.setPassword(request.getParameter("pass"));
        Perfil perfil = new Perfil();
        perfil.setId(Integer.parseInt(request.getParameter("perfil")));
        user.setIdPerfil(perfil);
        user.setNumeroDocumento(request.getParameter("documento"));
        user.setCupoDinero(Integer.parseInt(request.getParameter("dinero")));
        Estados estados = new Estados();
        estados.setId(Integer.parseInt(request.getParameter("estado")));
        user.setIdEstado(estados);
        usuariosE.create(user);
        SesionExpert.getInstance().setSession(request, "listas",cargarObjetos());
        SesionExpert.getInstance().redireccionarMensaje(response, "Usuario "+user.getNombre()+" Creado Exitosamente", (String) SesionExpert.getInstance().getSession(request, "redirecionPagina"));
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public UtilDTO cargarObjetos(){
        
       List<Apuesta> apuestas = apuestaE.findAll();
       List<Boletas> boletas = boletasE.findAll();
       List<Campeonato> campeonatos = campeonatosE.findAll();
       List<Deporte> deportes = deportesE.findAll();
       List<EquipoJugador> equipoJugadores = equipoJugadoresE.findAll();
       List<Estados> estados = estadosE.findAll();
       List<Parametros> parametros = parametrosE.findAll();
       List<Perfil> perfiles = perfilesE.findAll();
       List<Premios> premios = premiosE.findAll();
       List<Rifa> rifas = rifasE.findAll();
       List<VentaApuestas> ventaApuestas = ventaApuestasE.findAll();
       List<VentaRifas> ventaRifas = ventaRifasE.findAll();
       List<Usuario> usuarios = usuariosE.findAll();
       List<Partido> partidos =  partidosE.findAll();
       List<Marcador> marcadores = marcadoresE.findAll();
       UtilDTO inDTO = new UtilDTO();
       inDTO.setApuestas(apuestas);
       inDTO.setBoletas(boletas);
       inDTO.setCampeonatos(campeonatos);
       inDTO.setDeportes(deportes);
       inDTO.setEquipoJugadores(equipoJugadores);
       inDTO.setEstados(estados);
       inDTO.setParametros(parametros);
       inDTO.setPerfiles(perfiles);
       inDTO.setPremios(premios);
       inDTO.setRifas(rifas);
       inDTO.setVentaApuestas(ventaApuestas);
       inDTO.setVentaRifas(ventaRifas);
       inDTO.setUsuarios(usuarios);
       inDTO.setPartidos(partidos);
       inDTO.setMarcadores(marcadores);
       return inDTO;
    }
    
}
