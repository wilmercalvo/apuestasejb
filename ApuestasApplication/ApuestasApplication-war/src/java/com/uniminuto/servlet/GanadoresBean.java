/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.servlet;

import com.uniminuto.entity.Parametros;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.VentaApuestas;
import com.uniminuto.entity.VentaRifas;
import com.uniminuto.session.ParametrosFacadeLocal;
import com.uniminuto.session.VentaApuestasFacadeLocal;
import com.uniminuto.session.VentaRifasFacadeLocal;
import com.uniminuto.util.DateExpert;
import com.uniminuto.util.GeneralUtil;
import com.uniminuto.util.SesionExpert;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author User
 */
@WebServlet(name = "GanadoresBean", urlPatterns = {"/GanadoresBean"})
public class GanadoresBean extends HttpServlet {

    @EJB
    private ParametrosFacadeLocal parametrosE;
    @EJB
    private VentaApuestasFacadeLocal ventaApuestasE;
    @EJB
    private VentaRifasFacadeLocal ventaRifasE;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GanadoresBean</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GanadoresBean at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        List<Parametros> parametros = parametrosE.findAll();
        for(Parametros param: parametros){
            if(param.getId() == 1){ //parametros ganadores
                if(param.getDescripcion().equalsIgnoreCase("true")){
                    String flag = request.getParameter("type");
                    if(flag.equals("1")){//rifa
                        String datosGanadores = "{\"ganadores\": " + ganadoresRifa()+ " }";
                        System.out.println("Ganadores " + datosGanadores);
                        SesionExpert.getInstance().mensaje(response,datosGanadores);
                    }else if(flag.equals("2")){ //apuesta
                        int deporte = request.getParameter("deporte") != null ?  Integer.parseInt(request.getParameter("deporte")) : 0;
                        int campeonato = request.getParameter("campeonato") != null ?  Integer.parseInt(request.getParameter("campeonato")) : 0;
                        int partido = request.getParameter("partido") != null ? Integer.parseInt(request.getParameter("partido")) : 0;
                        
                        String datosGanadores = "{\"ganadores\": " + ganadoresApuesta(deporte, campeonato, partido)+ " }";
                        System.out.println("Ganadores " + datosGanadores);
                        SesionExpert.getInstance().mensaje(response,datosGanadores);
                    }
                    
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String ganadoresRifa(){
        JSONArray data = new JSONArray();
        List<VentaRifas> rifaV = ventaRifasE.findAll();
        for (VentaRifas rifa1 : rifaV) {
            if(rifa1.getResultado().equalsIgnoreCase("GANADO") && DateExpert.getInstance().diferenciaFechaDias(DateExpert.getInstance().fechaDate(rifa1.getIdRifa().getFechaRifa())) == 0){
                try{
                    JSONObject result = new JSONObject();
                    result.put("nombre",rifa1.getIdUsuario().getNombre());
                    result.put("evento",rifa1.getIdRifa().getNombre());
                    result.put("cc",GeneralUtil.getInstance().maskProductNumber(rifa1.getIdUsuario().getNumeroDocumento()));
                    data.add(result);
                }catch(Throwable ex){
                   System.out.println("se presento el error [ "+ex.getMessage()+" ] en el metodo ");
                }
            }
        }
        return data.toString();
    }
    
    public String ganadoresApuesta(int deporte, int campeonato, int partido){
        JSONArray data = new JSONArray();
        List<VentaApuestas> apuestaV = ventaApuestasE.findAll();
        for(VentaApuestas apuesta1 : apuestaV) {
            if(apuesta1.getResultado().equalsIgnoreCase("GANADO") && DateExpert.getInstance().diferenciaFechaDias(DateExpert.getInstance().fechaDate(apuesta1.getIdApuesta().getIdPartido().getFechaPartido())) == 0){
                try{
                    Partido pp = apuesta1.getIdApuesta().getIdPartido();
                    if(deporte > 0 && campeonato > 0 && partido > 0){
                        if(pp.getIdDeporte().getId() == deporte && 
                                (pp.getIdEquipoLocal().getIdCampeonato().getId() == campeonato || pp.getIdEquipoVisitante().getIdCampeonato().getId() == campeonato) &&
                                 pp.getId() == partido){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 data.add(result);
                        }
                        
                    }else if(campeonato > 0 && partido > 0){
                        if((pp.getIdEquipoLocal().getIdCampeonato().getId() == campeonato || pp.getIdEquipoVisitante().getIdCampeonato().getId() == campeonato) &&
                                 pp.getId() == partido){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 data.add(result);
                        }
                        
                    }else if(deporte > 0 && partido > 0){
                        if(pp.getIdDeporte().getId() == deporte && 
                                 pp.getId() == partido){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 data.add(result);
                        }
                    }else if(deporte > 0 && campeonato > 0){
                        if(pp.getIdDeporte().getId() == deporte && 
                                (pp.getIdEquipoLocal().getIdCampeonato().getId() == campeonato || pp.getIdEquipoVisitante().getIdCampeonato().getId() == campeonato)){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 data.add(result);
                        }
                    }else if(deporte  > 0){
                        if(pp.getIdDeporte().getId() == deporte){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 data.add(result);
                        }
                    }else if(campeonato  > 0){
                        if((pp.getIdEquipoLocal().getIdCampeonato().getId() == campeonato || pp.getIdEquipoVisitante().getIdCampeonato().getId() == campeonato)){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 data.add(result);
                        }
                    }else if(partido > 0){
                        if(pp.getId() == partido){
                                 JSONObject result = new JSONObject();
                                 result.put("nombre",apuesta1.getIdUsuario().getNombre());
                                 result.put("evento",apuesta1.getIdApuesta().getNombre());
                                 result.put("cc",GeneralUtil.getInstance().maskProductNumber(apuesta1.getIdUsuario().getNumeroDocumento()));
                                 data.add(result);
                        }
                    }
                }catch(Throwable ex){
                   System.out.println("se presento el error [ "+ex.getMessage()+" ] en el metodo ");
                }
            }
        }
        return data.toString();
    }
}
