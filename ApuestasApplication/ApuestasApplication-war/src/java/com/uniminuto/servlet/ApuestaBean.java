/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.servlet;

import com.uniminuto.entity.Apuesta;
import com.uniminuto.entity.Boletas;
import com.uniminuto.entity.Campeonato;
import com.uniminuto.entity.Deporte;
import com.uniminuto.entity.EquipoJugador;
import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Marcador;
import com.uniminuto.entity.Parametros;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.Perfil;
import com.uniminuto.entity.Premios;
import com.uniminuto.entity.Rifa;
import com.uniminuto.entity.Usuario;
import com.uniminuto.entity.VentaApuestas;
import com.uniminuto.entity.VentaRifas;
import com.uniminuto.session.ApuestaFacadeLocal;
import com.uniminuto.session.BoletasFacadeLocal;
import com.uniminuto.session.CampeonatoFacadeLocal;
import com.uniminuto.session.DeporteFacadeLocal;
import com.uniminuto.session.EquipoJugadorFacadeLocal;
import com.uniminuto.session.EstadosFacadeLocal;
import com.uniminuto.session.MarcadorFacadeLocal;
import com.uniminuto.session.ParametrosFacadeLocal;
import com.uniminuto.session.PartidoFacadeLocal;
import com.uniminuto.session.PerfilFacadeLocal;
import com.uniminuto.session.PremiosFacadeLocal;
import com.uniminuto.session.RifaFacadeLocal;
import com.uniminuto.session.UsuarioFacadeLocal;
import com.uniminuto.session.VentaApuestasFacadeLocal;
import com.uniminuto.session.VentaRifasFacadeLocal;
import com.uniminuto.util.DateExpert;
import com.uniminuto.util.SesionExpert;
import com.uniminuto.util.UtilDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author User
 */
public class ApuestaBean extends HttpServlet {

        @EJB
        ApuestaFacadeLocal apuestaE;
        @EJB
        BoletasFacadeLocal boletasE;
        @EJB
        CampeonatoFacadeLocal campeonatosE;
        @EJB
        DeporteFacadeLocal deportesE;
        @EJB
        EquipoJugadorFacadeLocal equipoJugadoresE;
        @EJB
        EstadosFacadeLocal estadosE;
        @EJB
        ParametrosFacadeLocal parametrosE;
        @EJB
        PerfilFacadeLocal perfilesE;
        @EJB
        PremiosFacadeLocal premiosE;
        @EJB
        RifaFacadeLocal rifasE;
        @EJB
        VentaApuestasFacadeLocal ventaApuestasE;
        @EJB
        VentaRifasFacadeLocal ventaRifasE;
        @EJB
        UsuarioFacadeLocal usuariosE;
        @EJB
        PartidoFacadeLocal partidosE;
        @EJB
        MarcadorFacadeLocal marcadoresE;
    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ApuestaBean</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ApuestaBean at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
          apuesta(request);
        rifa(request);
        SesionExpert.getInstance().setSession(request, "listas",cargarObjetos()); 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void apuesta(HttpServletRequest request){
        List<Marcador> marcadores =   marcadoresE.findAll();
        List<VentaApuestas> ventaApuestas =  ventaApuestasE.findAll();
        List<Partido> partidos =  partidosE.findAll();
        int numMarcadores = marcadores.size();
        Random aleatorio = new Random(System.currentTimeMillis());
        for (Partido partido : partidos) {
            if(partido.getMarcadorJuego() == 999 && (partido.getIdEstado().getId() == 1 || partido.getIdEstado().getId() == 5)){ //ESTADOS PARA JUGAR: 1 activo, 5 cerrado 
             
               int dias = DateExpert.getInstance().diferenciaFecha(DateExpert.getInstance().fechaDate(partido.getFechaPartido()));
               if(dias <= 0){
                   int intAletorio = aleatorio.nextInt(numMarcadores);
                   partido.setMarcadorJuego(intAletorio + 1);
                   Estados estados = new Estados();
                   estados.setId(3);
                   partido.setIdEstado(estados);
                   partidosE.edit(partido);
               }
            }
            
        }
        for (VentaApuestas ventaApuesta : ventaApuestas) {
            if(ventaApuesta.getIdApuesta().getIdPartido().getMarcadorJuego() != 999 && (ventaApuesta.getIdEstado().getId() == 1)){ 
                int marcadorJuego = ventaApuesta.getIdApuesta().getIdPartido().getMarcadorJuego();
                int marcadorApostado =  ventaApuesta.getMarcadorJuego().getId();
                if(marcadorJuego == marcadorApostado){
                    ventaApuesta.setResultado("GANADO");
                }else{
                    ventaApuesta.setResultado("PERDIDO");
                }
                Estados estados = new Estados();
                estados.setId(3);
                ventaApuesta.setIdEstado(estados);
                ventaApuestasE.edit(ventaApuesta);
            }
        }
    }
    
    public void rifa(HttpServletRequest request){
        List<Rifa> rifa =  rifasE.findAll();
        List<VentaRifas> ventaRifas = ventaRifasE.findAll();
        Random aleatorio = new Random(System.currentTimeMillis());
        for (Rifa rifa1 : rifa) {
            int dias = DateExpert.getInstance().diferenciaFecha(DateExpert.getInstance().fechaDate(rifa1.getFechaRifa()));
               if(dias <= 0){
                    if("Pendiente".equalsIgnoreCase(rifa1.getResultado()) && (rifa1.getIdEstado().getId() == 1 || rifa1.getIdEstado().getId() == 5 )){ //ESTADOS PARA JUGAR: 1 activo, 5 cerrado 
                        int intAletorio = aleatorio.nextInt(rifa1.getNumeroMaxRifa());
                        rifa1.setResultado(String.valueOf(intAletorio + 1));
                        Estados estados = new Estados();
                        estados.setId(3);
                        rifa1.setIdEstado(estados);
                        rifasE.edit(rifa1);
                    }
            }
        }
        
        for (VentaRifas ventaRifa : ventaRifas) {
            if(!"Pendiente".equalsIgnoreCase(ventaRifa.getIdRifa().getResultado()) && (ventaRifa.getIdEstado().getId() == 1)){
                String resultadoRifa = ventaRifa.getIdRifa().getResultado();
                String resultadoVentaRifa = String.valueOf(ventaRifa.getMarcadorJuego());
                if(resultadoRifa.equalsIgnoreCase(resultadoVentaRifa)){
                    ventaRifa.setResultado("GANADO");
                }else{
                    ventaRifa.setResultado("PERDIDO");
                }
                Estados estados = new Estados();
                estados.setId(3);
                ventaRifa.setIdEstado(estados);
                ventaRifasE.edit(ventaRifa);
            }
        }
    }
    
    public UtilDTO cargarObjetos(){
        
       List<Apuesta> apuestas = apuestaE.findAll();
       List<Boletas> boletas = boletasE.findAll();
       List<Campeonato> campeonatos = campeonatosE.findAll();
       List<Deporte> deportes = deportesE.findAll();
       List<EquipoJugador> equipoJugadores = equipoJugadoresE.findAll();
       List<Estados> estados = estadosE.findAll();
       List<Parametros> parametros = parametrosE.findAll();
       List<Perfil> perfiles = perfilesE.findAll();
       List<Premios> premios = premiosE.findAll();
       List<Rifa> rifas = rifasE.findAll();
       List<VentaApuestas> ventaApuestas = ventaApuestasE.findAll();
       List<VentaRifas> ventaRifas = ventaRifasE.findAll();
       List<Usuario> usuarios = usuariosE.findAll();
       List<Partido> partidos =  partidosE.findAll();
       List<Marcador> marcadores = marcadoresE.findAll();
       UtilDTO inDTO = new UtilDTO();
       inDTO.setApuestas(apuestas);
       inDTO.setBoletas(boletas);
       inDTO.setCampeonatos(campeonatos);
       inDTO.setDeportes(deportes);
       inDTO.setEquipoJugadores(equipoJugadores);
       inDTO.setEstados(estados);
       inDTO.setParametros(parametros);
       inDTO.setPerfiles(perfiles);
       inDTO.setPremios(premios);
       inDTO.setRifas(rifas);
       inDTO.setVentaApuestas(ventaApuestas);
       inDTO.setVentaRifas(ventaRifas);
       inDTO.setUsuarios(usuarios);
       inDTO.setPartidos(partidos);
       inDTO.setMarcadores(marcadores);
       return inDTO;
    }
    
}
