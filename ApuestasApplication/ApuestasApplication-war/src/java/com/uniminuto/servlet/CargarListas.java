/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.servlet;

import com.uniminuto.entity.Campeonato;
import com.uniminuto.entity.Deporte;
import com.uniminuto.entity.Parametros;
import com.uniminuto.entity.Partido;
import com.uniminuto.session.CampeonatoFacadeLocal;
import com.uniminuto.session.DeporteFacadeLocal;
import com.uniminuto.session.ParametrosFacadeLocal;
import com.uniminuto.session.PartidoFacadeLocal;
import com.uniminuto.util.SesionExpert;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author User
 */
public class CargarListas extends HttpServlet {
   
    @EJB
    private CampeonatoFacadeLocal campeonatosE;
    @EJB
    private DeporteFacadeLocal deportesE;
    @EJB
    private ParametrosFacadeLocal parametrosE;
    @EJB
    private PartidoFacadeLocal partidosE;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CargarListas</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CargarListas at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        String respuesta = "{\"type\": 1, \"campeonato\": " + getCampeonato() + ", \"deporte\": " + getDeporte() +", \"partido\": " + getPartido() +", \"parametro\": " + getParametro()+"}";
        SesionExpert.getInstance().mensaje(response,respuesta);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       // processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public String getCampeonato(){
            List<Campeonato> obj =  campeonatosE.findAll();
            JSONArray data = new JSONArray();
            for (Campeonato object : obj) {
                    JSONObject result = new JSONObject();
                    result.put("id", object.getId().toString());
                    result.put("valor", object.getDescripcion());
                    data.add(result);
        }
        return data.toString();    
    }
    public String getDeporte(){
            List<Deporte> obj =  deportesE.findAll();
            JSONArray data = new JSONArray();
            for (Deporte object : obj) {
                    JSONObject result = new JSONObject();
                    result.put("id", object.getId().toString());
                    result.put("valor", object.getDescripcion());
                    data.add(result);
        }
        return data.toString();    
    }
    public String getPartido(){
            List<Partido> obj =  partidosE.findAll();
            JSONArray data = new JSONArray();
            for (Partido object : obj) {
                    JSONObject result = new JSONObject();
                    result.put("id", object.getId().toString());
                    result.put("valor", object.getNombre());
                    data.add(result);
        }
        return data.toString();    
    }
    public String getParametro(){
            List<Parametros> obj =  parametrosE.findAll();
            JSONArray data = new JSONArray();
            for (Parametros object : obj) {
                    JSONObject result = new JSONObject();
                    result.put("id", object.getId().toString());
                    result.put("valor", object.getDescripcion());
                    data.add(result);
        }
        return data.toString();    
    }
}
