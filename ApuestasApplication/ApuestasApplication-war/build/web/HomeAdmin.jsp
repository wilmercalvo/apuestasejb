<%-- 
    Document   : index
    Created on : 23/02/2019, 09:46:08 PM
    Author     : User
--%>

<%@page import="com.uniminuto.entity.VentaRifas"%>
<%@page import="com.uniminuto.entity.Rifa"%>
<%@page import="com.uniminuto.entity.Premios"%>
<%@page import="com.uniminuto.entity.Marcador"%>
<%@page import="com.uniminuto.entity.VentaApuestas"%>
<%@page import="com.uniminuto.entity.Apuesta"%>
<%@page import="com.uniminuto.entity.Partido"%>
<%@page import="com.uniminuto.entity.EquipoJugador"%>
<%@page import="com.uniminuto.entity.Campeonato"%>
<%@page import="com.uniminuto.entity.Deporte"%>
<%@page import="com.uniminuto.entity.Usuario"%>
<%@page import="com.uniminuto.entity.Estados"%>
<%@page import="com.uniminuto.entity.Perfil"%>
<%@page import="com.uniminuto.util.UtilDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>..::UNIAPUESTAS::..</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
        <link rel="stylesheet" type="text/css" href="css/slider.css" media="all" />
        <!-- jQuery -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <!-- FlexSlider -->
        <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
        <script type="text/javascript" src="js/Slider.js"></script>
        <script type="text/javascript" src="js/home.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.full.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css" rel="stylesheet"/>

        <LINK href="css/Menu.css" rel="stylesheet" type="text/css">
        <%HttpSession sesion = request.getSession();
          UtilDTO outDTO = (UtilDTO) sesion.getAttribute("listas");
          String nombreS = (String) sesion.getAttribute("nombre");
        %>
    </head>
    
    <body onLoad="setInterval('gestionarJuegos()',60000);">
        <div id="div-contenedor">
        <div class="slider_container">
		<div class="flexslider">
	      <ul class="slides">
	    	<li>
	    		<a href="#"><img src="images/slider/slide1.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Party Hairstyles</h2><p>Find the perfect hairstyle a la Lauren Conrad for every type of festive fête.</p></div>-->
                </div>
	    	</li>
	    	<li>
	    		<a href="#"><img src="images/slider/slide2.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Beautiful Hairstyle</h2><p>The latest hairstyles and instructions on how to create them here. Total Beauty has your complete hairstyles guide</p></div>-->
                </div>
	    	</li>
	    	<li>
	    		<a href="#"><img src="images/slider/slide3.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Party Dresses</h2><p>If you are looking for something a little special for your big night out, check out Rare London's collection of stunning party dresses</p></div>-->
                </div>
	    	</li>
	    	<li >
	    		<a href="#"><img src="images/slider/slide4.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Bodycon Dresses</h2><p>The bodycon dress is a key silhouette for this season's party girl; from sleek colour-block panelling to geometric prints the bodycon.</p></div>-->
                </div>
	    	</li>
	    </ul>
	  </div>
	  </div>
       
        <header class="page-header1">
        <div class="center-contents1">
          <nav class="page-nav1">
            <ul id="menu11">
                
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('nuevoUsuario');">Nuevo Usuario</a></li>
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('gestionarPartido');">Gestionar Partido</a></li>
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('gestionarApuesta');" >Gestionar Apuesta</a></li>
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('boletasApuesta');" >Boletas Apuesta</a></li>
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('gestionarRifa');" >Gestionar Rifa</a></li>
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('boletasRifa');" >Boletas Rifa</a></li>
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('reportes');" >Reportes</a></li>
              <li class="items1" style="float:right"><a class="active1" href="#" onClick="logout();">LogOut</a></li>
            </ul>
          </nav>
        </div>
        </header>    
            <section>
                <div id="principal">
                    <div id="datos"> 
                        <p id="datoTitulo" >Bienvenid@ <%=nombreS%></p>
                    </div>
                    <div id="nuevoUsuario" class="ventanas" style="display: none;">
                        <form action="AdminCrearUsuarioBean" method="post">
                            <br>
                            <table>
                                <tr><th>NUEVO USUARIO</th></tr>
                                <tr>
                                    <td>
                                        <label>Nombre:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="nombre" id="nombre" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Usuario:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="user" id="user" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Contraseña:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="pass" id="pass" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Perfil</label>
                                    </td>
                                    <td>
                                        <select name="perfil" id="perfil" required="true">
                                            <%for (Perfil elem : outDTO.getPerfiles() ) {
                                                int id = elem.getId();
                                                String valor = elem.getDescripcion();
                                                    %>
                                                    <option value="<%=id%>"><%=valor%></option>
                                                    <%
                                                }
                                            %>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Numero Documento:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="documento" id="documento" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Cupo de Dinero:</label>
                                    </td>
                                    <td>
                                        <input type="number" name="dinero" id="dinero" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Estado</label>
                                    </td>
                                    <td>
                                        <select name="estado" id="estado" required="true">
                                            <%for (Estados elem : outDTO.getEstados() ) {
                                                int id = elem.getId();
                                                String valor = elem.getDescripcion();
                                                    %>
                                                    <option value="<%=id%>"><%=valor%></option>
                                                    <%
                                                }
                                            %>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input class="boton" type="submit" value="Crear"/>
                                    </td>
                                </tr>
                            </table>
                            
                            
                        </form>
                       <br>
                       <hr>
                       <div style="height: 400px; overflow: scroll;">
                    <table>
                        <tr><th>REGISTRO USUARIO</th></tr>
                        <tr>
                            <td>
                                <label>Nombre</label>
                            </td>
                            <td>
                                <label>Usuario</label>
                            </td>
                            <td>
                                <label>Contraseña</label>
                            </td>
                            <td>
                                <label>Perfil</label>
                            </td>
                            <td>
                                <label>Numero Documento</label>
                            </td>
                            <td>
                                <label>Cupo de Dinero</label>
                            </td>
                            <td>
                                <label>Estado</label>
                            </td>
                            <td></td>
                        </tr>
                    <%
                        for (Usuario usuario : outDTO.getUsuarios()) {
                            int iduser = usuario.getId();
                            String  nombre = usuario.getNombre();
                            String  userName = usuario.getUsuario();
                            String  password = usuario.getPassword();
                            int perfil = usuario.getIdPerfil().getId();
                            String identificacion = usuario.getNumeroDocumento();
                            int dinero = usuario.getCupoDinero();
                            int estado = usuario.getIdEstado().getId();
                        %>
                        <form action="AdminModificarUsuarioBean" method="post">                    
                            
                                <tr>
                                    <td>
                                        <input type="hidden" value="<%=iduser%>" name="id" id="id" />
                                        <input type="text" name="nombre" id="nombre" required="true" value="<%=nombre%>"/>
                                    </td>
                                    <td>
                                        <input type="text" name="user" id="user" required="true" value="<%=userName%>" />
                                    </td>
                                    <td>
                                        <input type="text" name="pass" id="pass" required="true"  value="<%=password%>" />
                                    </td>
                                    <td>
                                        <select name="perfil" id="perfil" required="true">
                                            <%for (Perfil perfilAll : outDTO.getPerfiles() ) {
                                                int id = perfilAll.getId();
                                                String valor = perfilAll.getDescripcion();
                                                    if(perfil == id){
                                                      %>
                                                      <option value="<%=id%>" selected="true"><%=valor%></option>
                                                       <%  
                                                    }else{
                                                        %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                        <%
                                                    }
                                                }
                                            %>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="documento" id="documento" required="true" value="<%=identificacion%>" />
                                    </td>
                                    <td>
                                        <input type="number" name="dinero" id="dinero" required="true" value="<%=dinero%>" />
                                    </td>
                                    <td>
                                        <select name="estado" id="estado" required="true">
                                            <%for (Estados elem : outDTO.getEstados() ) {
                                                int id = elem.getId();
                                                String valor = elem.getDescripcion();
                                                if(estado == id){
                                                    %>
                                                    <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                }else{
                                                     %>
                                                     <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                }
                                              }
                                            %>
                                        </select>
                                    </td>
                                    <td>
                                        <input class="boton" type="submit" value="Modificar"/>
                                    </td>
                                </tr>
                        </form>
                       <%
                       }
                       %>    
                       </table>
                    </div>
                    </div>
                       <br>                                      
                       <div id="gestionarPartido" class="ventanas" style="display: none;">
                           <table> <!-- tabla principal-->
                               <tr>
                                   <td valign="top" style="border: 1px solid black;">
                                       
                                       <table>
                                           <tr><th>NUEVO DEPORTE</th></tr>
                                            <tr>
                                                <td>
                                                    <label>Nombre Deporte:</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" name="depte" id="depte" required="true"/>
                                                </td>
                                            </tr>
                                            <tr>
                                            <td valign="bottom">
                                                <input type="button" value="Guardar" onClick="gestionarFormulario('AdminCrearDeporteBean','depte');" id="btnDepte" class="boton"/>
                                            </td>
                                            </tr>
                                        </table>
                                   </td>
                                   <td valign="top" style="border: 1px solid black;">
                                       <table>
                                           <tr><th>NUEVO CAMPEONATO</th></tr>
                                            <tr>
                                                <td>
                                                    <label>Nombre Campeonato:</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" name="depte" id="campeonatoId" required="true"/>
                                                </td>
                                            </tr>
                                            <tr>
                                            <td valign="bottom">
                                                <input type="button" value="Guardar" onClick="gestionarFormulario('AdminCrearCampeonatoBean','campeonatoId');" id="btnDepte" class="boton"/>
                                            </td>
                                            </tr>
                                        </table>
                                   </td>
                                   <td valign="top" style="border: 1px solid black;">
                                       <table>
                                           <tr><th>NUEVO EQUIPO</th></tr>
                                            <tr>
                                                <td>
                                                    <label>Nombre Equipo:</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" name="nomEquipo" id="nomEquipo" required="true"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>Campeonato:</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                     <select name="campeonato" id="campeonato" required="true">
                                                         <%for (Campeonato elem : outDTO.getCampeonatos() ) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>"  ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                                     </select>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                            <td valign="bottom">
                                                <input type="button" value="Guardar" id="btnDepte" class="boton" onClick="gestionarFormulario('AdminCrearEquipoBean','nomEquipo,campeonato');"/>
                                            </td>
                                            </tr>
                                        </table>
                                   </td>
                                   <td valign="top" style="border: 1px solid black;">
                                       <table>
                                           <tr><th>NUEVA APUESTA</th></tr>
                                           <tr>
                                               <td>
                                                   <label>Nombre</label>
                                               </td>
                                               <td>
                                                   <input type="text" id="nombrePartido" name="nombrePartido" />
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>
                                                   <label>Deporte</label>
                                               </td>
                                               <td>
                                                   <select name="deportePartido" id="deportePartido" required="true">
                                                         <%for (Deporte elem : outDTO.getDeportes() ) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>"  ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                                     </select>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>
                                                   <label>Equipo Local</label>
                                               </td>
                                               <td>
                                                   <select name="equipoLocal" id="equipoLocal" required="true">
                                                       <%for (EquipoJugador elem : outDTO.getEquipoJugadores() ) {
                                                         int id = elem.getId();
                                                         String valor = elem.getNombre();
                                                          %>
                                                             <option value="<%=id%>"  ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                                     </select>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>
                                                   <label>Equipo Visitante</label>
                                               </td>
                                               <td>
                                                   <select name="equipoVisitante" id="equipoVisitante" required="true">
                                                       <%for (EquipoJugador elem : outDTO.getEquipoJugadores() ) {
                                                         int id = elem.getId();
                                                         String valor = elem.getNombre();
                                                          %>
                                                             <option value="<%=id%>"  ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                                     </select>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>
                                                   <label>Fecha Partido</label>
                                               </td>
                                               <td>
                                                   <input type="text" id="fechaPartido" name="fechaPartido" class="datepicker"/>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>
                                                   <label>Estado Partido</label>
                                               </td>
                                               <td>
                                                   <select name="estadoEquipo" id="estadoEquipo" required="true">
                                                       <%for (Estados elem : outDTO.getEstados()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>"  ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                                     </select>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td valign="bottom">
                                                   <input type="button"  value="Guardar" class="boton" onClick="gestionarFormulario('AdminCrearPartidoBean','nombrePartido,deportePartido,equipoLocal,equipoVisitante,fechaPartido,estadoEquipo');"/>
                                               </td>
                                           </tr>
                                       </table>
                                   </td>
                               </tr>
                           </table>
                            <hr>
                            <div style="height: 400px; overflow: scroll;">
                                <br>
                                <table>
                                    <%
                                        if(outDTO.getPartidos().size() > 0){
                                        %>
                                    <tr><th>REGISTRO PARTIDOS</th></tr>
                                    <tr>
                                        <td>
                                           <label>Nombre</label>
                                        </td>
                                        <td>
                                           <label>Deporte</label>
                                        </td>
                                        <td>
                                            <label>Equipo Local</label>
                                        </td>
                                        <td>
                                            <label>Equipo Visitante</label>
                                        </td>
                                        <td>
                                            <label>Fecha Partido</label>
                                        </td>
                                        <td>
                                            <label>Resultado</label>
                                        </td>
                                        <td>
                                            <label>Estado Partido</label>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <%
                                        }
                                        for (Partido elem : outDTO.getPartidos()) { 
                                            int idP = elem.getId();
                                            String nombre = elem.getNombre();
                                            int idDeporte = elem.getIdDeporte().getId();
                                            int idEquipLocal = elem.getIdEquipoLocal().getId();
                                            int idEquipVisit = elem.getIdEquipoVisitante().getId();
                                            String fecha = elem.getFechaPartido();
                                            int idEstado = elem.getIdEstado().getId();
                                            int marcador = elem.getMarcadorJuego();
                                            %>
                                            <form action="AdminModificarPartidoBean" method="post">
                                                <tr>
                                                <input type="hidden" value="<%=idP%>" id="idP" name="idP"/>
                                               <td>
                                                   <input type="text" id="nombrePartidoP" name="nombrePartidoP"  value="<%=nombre%>"/>
                                               </td>
                                               
                                               <td>
                                                   <select name="deportePartidoP" id="deportePartidoP" required="true">
                                                         <%for (Deporte elemP : outDTO.getDeportes() ) {
                                                         int id = elemP.getId();
                                                         String valor = elemP.getDescripcion();
                                                          if(idDeporte == id){
                                                                %>
                                                                <option value="<%=id%>"  ><%=valor%></option>
                                                                <%
                                                            }else{
                                                                 %>
                                                                 <option value="<%=id%>"><%=valor%></option>
                                                                 <%
                                                            }
                                                       }
                                                     %>
                                                     </select>
                                               </td>
                                               
                                               <td>
                                                   <select name="equipoLocalP" id="equipoLocalP" required="true">
                                                       <%for (EquipoJugador elemP : outDTO.getEquipoJugadores() ) {
                                                         int id = elemP.getId();
                                                         String valor = elemP.getNombre();
                                                          if(idEquipLocal == id){
                                                                %>
                                                                <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                                <%
                                                            }else{
                                                                 %>
                                                                 <option value="<%=id%>"><%=valor%></option>
                                                                 <%
                                                            }
                                                       }
                                                     %>
                                                     </select>
                                               </td>
                                               
                                               <td>
                                                   <select name="equipoVisitanteP" id="equipoVisitanteP" required="true">
                                                       <%for (EquipoJugador elemP : outDTO.getEquipoJugadores() ) {
                                                         int id = elemP.getId();
                                                         String valor = elemP.getNombre();
                                                          if(idEquipVisit == id){
                                                                %>
                                                                <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                                <%
                                                            }else{
                                                                 %>
                                                                 <option value="<%=id%>"><%=valor%></option>
                                                                 <%
                                                            }
                                                       }
                                                     %>
                                                     </select>
                                               </td>
                                               
                                               <td>
                                                   <input type="text" class="datepicker" id="fechaPartidoP" name="fechaPartidoP" value="<%=fecha%>"/>
                                               </td>
                                               <td>
                                                       <%
                                                         if(marcador == 999){
                                                             %>
                                                             <input type="text" id="marcadorMs" name="marcadorMs" value="Pendiente" readonly="true"/>
                                                             <input type="hidden" id="marcadorM" name="marcadorM" value="999" />
                                                             <%
                                                         }else{  
                                                         for (Marcador elemP : outDTO.getMarcadores()) {
                                                         int id = elemP.getId();
                                                         String valor = elemP.getValor();
                                                         if(marcador == id){
                                                              %>
                                                              <input type="text" id="marcadorMs" name="marcadorMs" value="<%=valor%>" readonly="true"/>
                                                              <input type="hidden" id="marcadorM" name="marcadorM" value="<%=id%>" />
                                                             <%
                                                         }
                                                        }
                                                       }
                                                     %>
                                                   </td>
                                               
                                               <td>
                                                   <select name="estadoEquipoP" id="estadoEquipoP" required="true">
                                                       <%for (Estados elemP : outDTO.getEstados()) {
                                                         int id = elemP.getId();
                                                         String valor = elemP.getDescripcion();
                                                          if(idEstado == id){
                                                                %>
                                                                <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                                <%
                                                            }else{
                                                                 %>
                                                                 <option value="<%=id%>"><%=valor%></option>
                                                                 <%
                                                            }
                                                       }
                                                     %>
                                                     </select>
                                               </td>
                                               <td>
                                                   <input type="submit"  value="Guardar" class="boton" />
                                               </td>
                                           </tr>
                                           </form>
                                            <%
                                            }
                                    %>
                                </table>
                            </div>                         
                       </div>
                       <br>
                       <div id="gestionarApuesta" class="ventanas" style="display: none;">
                           <div>
                           <form action="AdminCrearApuestaBean" method="post">
                               <table>
                                   <tr><th>NUEVA APUESTA</th></tr>
                                   <tr>
                                       <td>
                                           <label>Nombre:</label>
                                       </td>
                                       <td>
                                           <input type="text" id="nombreApuesta" name="nombreApuesta" required="true"/>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <label>Descripcion:</label>
                                       </td>
                                       <td>
                                           <input type="text" id="descApuesta" name="descApuesta" required="true"/>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <label>Partido:</label>
                                       </td>
                                       <td>
                                           <select name="partidoApuesta" id="partidoApuesta" required="true">
                                                <%for (Partido elem : outDTO.getPartidos() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getNombre();
                                                    if(elem.getIdEstado().getId() == 1){
                                                    %>
                                                        <option value="<%=id%>"  ><%=valor%></option>
                                                    <%
                                                  }
                                                }
                                                %>
                                            </select>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <label>valor:</label>
                                       </td>
                                       <td>
                                           <input type="number" id="valorApuesta" name="valorApuesta"/>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <label>Estado:</label>
                                       </td>
                                       <td>
                                           <select name="estadoApuesta" id="estadoApuesta" required="true">
                                                <%for (Estados elem : outDTO.getEstados() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    %>
                                                        <option value="<%=id%>"  ><%=valor%></option>
                                                    <%
                                                  }
                                                %>
                                            </select>
                                       </td>
                                   </tr>
                                   <td>
                                       
                                   </td>
                                   <td>
                                       <input type="submit"  value="Guardar" class="boton" />
                                   </td>
                               </table>
                           </form>
                       </div>
                       <hr>
                       <div style="height: 400px; overflow: scroll;">
                           <br>
                           <table>
                               
                               <%
                                   if(outDTO.getApuestas().size() > 0){
                               %>
                               <tr><th>REGISTRO APUESTAS</th></tr>
                               <tr>
                                   <td>
                                       <label>Nombre</label>
                                   </td>
                                   <td>
                                       <label>Descripción</label>
                                   </td>
                                   <td>
                                       <label>Partido</label>
                                   </td>
                                   <td>
                                       <label>Valor</label>
                                   </td>
                                   <td>
                                       <label>Estado</label>
                                   </td>
                                   <td></td>
                               </tr>
                               <%
                                }
                                for (Apuesta apuestaP : outDTO.getApuestas()) {
                                    int idApuesta = apuestaP.getId();
                                    String  nombre = apuestaP.getNombre();
                                    String  descripcion = apuestaP.getDescripcion();
                                    int idPartido  = apuestaP.getIdPartido().getId();
                                    int valorApuesta = apuestaP.getValor();
                                    int idEstado = apuestaP.getIdEstado().getId();
                                    String fecha = apuestaP.getIdPartido().getFechaPartido();
                                    
                                %>
                               <tr>
                                  <form action="AdminModificarApuestaBean" method="post">
                                   <input type="hidden" value="<%=idApuesta%>" name="idApuestaM"/>
                                   <input type="hidden" value="<%=fecha%>" name="fechaEvento"/>
                                       <td>
                                           <input type="text" id="nombreApuestaM" name="nombreApuestaM" required="true"value="<%=nombre%>"/>
                                       </td>
                                       <td>
                                           <input type="text" id="descApuestaM" name="descApuestaM" required="true" value="<%=descripcion%>"/>
                                       </td>
                                       <td>
                                           <select name="partidoApuestaM" id="partidoApuestaM" required="true">
                                                <%for (Partido elem : outDTO.getPartidos() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getNombre();
                                                    if(idPartido == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <input type="number" id="valorApuestaM" name="valorApuestaM" value="<%=valorApuesta%>"/>
                                       </td>
                                       <td>
                                           <select name="estadoApuestaM" id="estadoApuestaM" required="true">
                                                <%for (Estados elem : outDTO.getEstados() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    if(idEstado == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                   <td>
                                       <input type="submit"  value="Guardar" class="boton" />
                                   </td>
                           </form> 
                               </tr>
                               <%
                                }%>
                           </table>
                       </div> 
                       </div>
                       
                      <div id="boletasApuesta" class="ventanas" style="display: none;">
                          <br>
                          <div>
                              <form action="AdminCrearVentaApuestaBean" method="post">
                                  <table>
                                      <tr><th>NUEVA BOLETA</th></tr>
                                      <tr>
                                          <td>
                                              <label>Apuesta</label>
                                          </td>
                                          <td>
                                              <select name="ventaApuestaApuesta" id="ventaApuestaApuesta" required="true">
                                                       <%for (Apuesta elem : outDTO.getApuestas()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getNombre() +" - "+elem.getIdPartido().getIdEquipoLocal().getNombre() +" vs "+elem.getIdPartido().getIdEquipoVisitante().getNombre();;
                                                         if(elem.getIdEstado().getId() == 1 && elem.getIdPartido().getIdEstado().getId() == 1){
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                         }
                                                       }
                                                     %>
                                               </select>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <label>Usuario</label>
                                          </td>
                                          <td>
                                              <select name="ventaUsuarioApuesta" id="ventaUsuarioApuesta" required="true">
                                                       <%for (Usuario elem : outDTO.getUsuarios()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getNombre();
                                                         if(id != 1){
                                                          %>
                                                             <option value="<%=id%>"  ><%=valor%></option>
                                                             <%
                                                          }       
                                                       }
                                                     %>
                                               </select>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <label>Valor Apostado</label>
                                          </td>
                                          <td>
                                              <input type="number" id="ventaValorApuesta" name="ventaValorApuesta" required="true"/>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <label>Marcador Apostado</label>
                                          </td>
                                          <td>
                                              <select name="marcadorApuesta" id="marcadorApuesta" required="true">
                                                  <%for (Marcador elem : outDTO.getMarcadores()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getValor();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <label>Estado</label>
                                          </td>
                                          <td>
                                              <select name="ventaEstadoApuesta" id="ventaEstadoApuesta" required="true">
                                                       <%for (Estados elem : outDTO.getEstados()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td></td>
                                          <td>
                                            <td>
                                                <input type="submit"  value="Guardar" class="boton" />
                                            </td>
                                          </td>
                                      </tr>
                                  </table>
                              </form>
                          </div>
                          
                          <hr>
                          <br>
                          <div style="height: 400px; overflow: scroll;">
                              <table>
                                  <%
                                    if(outDTO.getVentaApuestas().size() > 0){
                                  %>
                                  <tr><th>REGISTRO BOLETAS APUESTA</th></tr>
                               <tr>
                                   <td>
                                       <label>Apuesta</label>
                                   </td>
                                   <td>
                                       <label>Usuario</label>
                                   </td>
                                   <td>
                                       <label>Valor</label>
                                   </td>
                                   <td>
                                       <label>Marcador</label>
                                   </td>
                                   <td>
                                       <label>Resultado</label>
                                   </td>
                                   <td>
                                       <label>Estado</label>
                                   </td>
                                   <td></td>
                               </tr>
                               <%
                                }
                                for (VentaApuestas ventApues : outDTO.getVentaApuestas()) {
                                    int idVentaApuesta = ventApues.getId();
                                    int  idApuesta = ventApues.getIdApuesta().getId();
                                    int  idUsuario = ventApues.getIdUsuario().getId();
                                    int ventaValorApuesta  = ventApues.getValorApuesta();
                                    String resultado = ventApues.getResultado() == null ? "Pendiente" : ventApues.getResultado();
                                    int idEstado = ventApues.getIdEstado().getId();
                                    String fecha = ventApues.getIdApuesta().getIdPartido().getFechaPartido();
                                    int idMarcador = ventApues.getMarcadorJuego().getId();
                                    
                                %>
                               <tr>
                                  <form action="AdminModificarVentaApuestaBean" method="post">
                                   <input type="hidden" value="<%=idVentaApuesta%>" name="idVentaApuesta"/>
                                   <input type="hidden" value="<%=fecha%>" name="fechaEvento"/>
                                       <td>
                                           <select name="ventaApuestaApuestaM" id="ventaApuestaApuestaM" required="true">
                                                <%for (Apuesta elem : outDTO.getApuestas() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getNombre() +" - "+elem.getIdPartido().getIdEquipoLocal().getNombre() +" vs "+elem.getIdPartido().getIdEquipoVisitante().getNombre();;
                                                    if(idApuesta == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{  
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <select name="ventaUsuarioM" id="ventaUsuarioM" required="true">
                                                <%for (Usuario elem : outDTO.getUsuarios()) {
                                                    int id = elem.getId();
                                                    String valor = elem.getNombre();
                                                    if(id != 1){
                                                    if(idUsuario == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                   }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <input type="text" id="valorApuestaM" name="valorApuestaM" value="<%=ventaValorApuesta%>"/>
                                       </td>
                                       <td>
                                           <select name="marcadorApuestaM" id="marcadorApuestaM" required="true">
                                                <%for (Marcador elem : outDTO.getMarcadores() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getValor();
                                                    if(idMarcador == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <input type="text" id="resultado" name="resultado" value="<%=resultado%>" readonly="true"/>
                                       </td>
                                       <td>
                                           <select name="estadoApuestaM" id="estadoApuestaM" required="true">
                                                <%for (Estados elem : outDTO.getEstados() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    if(idEstado == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                   <td>
                                       <input type="submit"  value="Guardar" class="boton" />
                                   </td>
                           </form> 
                               </tr>
                               <%
                                }%>
                               </table>
                          </div>
                                               
                      </div>
                      
                      <div id="gestionarRifa" class="ventanas" style="display: none;">
                          <div id="gestionarPremiosRifa" style="float: left;">
                          <form >
                              <table>
                                  <tr><th>NUEVA PREMIO</th></tr>
                                  <tr>
                                      <td>
                                          <label>Premio</label>
                                      </td>
                                      <td>
                                          <input type="text" id="premio" name="premio" required="true"/>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td></td>
                                      <td>
                                          <input type="button" value="Guardar" onClick="gestionarFormulario('AdminCrearPremioBean','premio');"class="boton"/>
                                      </td>
                                  </tr>
                              </table>
                          </form>
                      </div>
                          <form action="AdminCrearRifaBean" method="post">
                              <table>
                                  <tr><th>NUEVA RIFA</th></tr>
                                  <tr>
                                      <td>
                                          <label>Nombre Rifa</label>
                                      </td>
                                      <td>
                                          <input type="text" required="true" id="nombre" name="nombre"/>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Descripción Rifa</label>
                                      </td>
                                      <td>
                                          <input type="text" required="true" id="descripcion" name="descripcion"/>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Valor</label>
                                      </td>
                                      <td>
                                          <input type="number" required="true" id="valor" name="valor"/>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Fecha Rifa</label>
                                      </td>
                                      <td>
                                          <input type="text" required="true" id="fecha" name="fecha" class="datepicker"/>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Premio Principal</label>
                                      </td>
                                      <td>
                                          <select name="premioPrincipal" id="premioPrincipal" required="true">
                                              <%for (Premios elem : outDTO.getPremios()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Segundo Premio</label>
                                      </td>
                                      <td>
                                          <select name="premioSecundario1" id="premioSecundario1" required="true">
                                              <%for (Premios elem : outDTO.getPremios()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Tercer Premio</label>
                                      </td>
                                      <td>
                                          <select name="premioSecundario2" id="premioSecundario2" required="true">
                                              <%for (Premios elem : outDTO.getPremios()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Cuarto Premio</label>
                                      </td>
                                      <td>
                                          <select name="premioSecundario3" id="premioSecundario3" required="true">
                                              <%for (Premios elem : outDTO.getPremios()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Quinto Premio</label>
                                      </td>
                                      <td>
                                          <select name="premioSecundario4" id="premioSecundario4" required="true">
                                              <%for (Premios elem : outDTO.getPremios()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Número de Cupos</label>
                                      </td>
                                      <td>
                                          <input type="number" id="cupos" name="cupos"  required="true"/>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Rango Rifa</label>
                                      </td>
                                      <td>
                                          <input type="number" id="rango" name="rango"  required="true"/>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Estado</label>
                                      </td>
                                      <td>
                                          <select name="estado" id="estado" required="true">
                                              <%for (Estados elem : outDTO.getEstados()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td></td>
                                      <td>
                                          <br>
                                          <input type="submit" value="Guardar" class="boton" />
                                      </td>
                                  </tr>
                              </table>
                          </form>
                          <hr>
                          <br>
                          <div style="height: 400px; overflow: scroll;">
                              <table>
                                  <%
                                    if(outDTO.getRifas().size() > 0){
                                  %>
                               <tr>
                                   <tr><th>REGISTRO RIFAS</th></tr>
                                   <td>
                                       <label>Nombre Rifa</label>
                                   </td>
                                   <td>
                                       <label>Descripcion</label>
                                   </td>
                                   <td>
                                       <label>Valor</label>
                                   </td>
                                   <td>
                                       <label>Resultado</label>
                                   </td>
                                   <td>
                                       <label>Fecha</label>
                                   </td>
                                   <td>
                                       <label>Cupos Participantes</label>
                                   </td>
                                   <td>
                                       <label>Cupos Actual</label>
                                   </td>
                                   <td>
                                       <label>Rango Rifa</label>
                                   </td>
                                   <td>
                                       <label>Premio Principal</label>
                                   </td>
                                   <td>
                                       <label>Segundo Premio</label>
                                   </td>
                                   <td>
                                       <label>Tercer Premio</label>
                                   </td>
                                   <td>
                                       <label>Cuarto Premio</label>
                                   </td>
                                   <td>
                                       <label>Quinto Premio</label>
                                   </td>
                                   
                                   <td>
                                       <label>Estado</label>
                                   </td>
                                   <td></td>
                               </tr>
                               <%
                                }
                                for (Rifa rifa : outDTO.getRifas()) {
                                    int idRifa = rifa.getId();
                                    String nombre   = rifa.getNombre();
                                    String descripcion = rifa.getDescripcion();
                                    int valorRifa = rifa.getValor();
                                    String resultado = rifa.getResultado() ;
                                    String fecha = rifa.getFechaRifa();
                                    int cupos =rifa.getCuposParticipantes();
                                    int rango = rifa.getNumeroMaxRifa();
                                    int cuposActual = rifa.getCuposActualParticipantes();
                                    int premioPrincipal = rifa.getPremioPrincipal().getId();
                                    int premioSecun1 = rifa.getIdPremioSecundario1().getId();
                                    int premioSecun2 = rifa.getIdPremioSecundario2().getId();
                                    int premioSecun3 = rifa.getIdPremioSecundario3().getId();
                                    int premioSecun4 = rifa.getIdPremioSecundario4().getId();
                                    int estado = rifa.getIdEstado().getId();
                                    
                                %>
                               <tr>
                                  <form action="AdminModificarRifaBean" method="post">
                                   <input type="hidden" value="<%=idRifa%>" name="idRifa"/>
                                   <td>
                                       <input type="text" value="<%=nombre%>" id="nombreM" name="nombreM" required="true" />
                                   </td>
                                   <td>
                                       <input type="text" value="<%=descripcion%>" id="descripcionM" name="descripcionM" required="true" />
                                   </td>
                                   <td>
                                       <input type="number" value="<%=valorRifa%>" id="valorM" name="valorM" required="true" />
                                   </td>
                                   <td>
                                       <input type="text" value="<%=resultado%>" id="resultadoM" name="resultadoM" required="true" readonly="true"/>
                                   </td>
                                   <td>
                                       <input type="text" value="<%=fecha%>" id="fechaM" name="fechaM" class="datepicker" required="true" />
                                   </td>
                                   <td>
                                       <input type="number" value="<%=cupos%>" id="cuposM" name="cuposM" required="true" />
                                   </td>
                                   <td>
                                       <input type="number" value="<%=cuposActual%>" id="cuposActualM" name="cuposActualM" required="true" />
                                   </td>
                                   <td>
                                       <input type="number" value="<%=rango%>" id="rangoM" name="rangoM" required="true" />
                                   </td>
                                       <td>
                                           <select name="premioP" id="premioP" required="true">
                                                <%for (Premios elem : outDTO.getPremios() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    if(premioPrincipal == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{  
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <select name="premioS1" id="premioS1" required="true">
                                                <%for (Premios elem : outDTO.getPremios() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    if(premioSecun1 == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{  
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <select name="premioS2" id="premioS2" required="true">
                                                <%for (Premios elem : outDTO.getPremios() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    if(premioSecun2 == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{  
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <select name="premioS3" id="premioS3" required="true">
                                                <%for (Premios elem : outDTO.getPremios() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    if(premioSecun3 == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{  
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <select name="premioS4" id="premioS4" required="true">
                                                <%for (Premios elem : outDTO.getPremios() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    if(premioSecun4 == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{  
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <select name="estadoM" id="estadoM" required="true">
                                                <%for (Estados elem : outDTO.getEstados() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    if(estado == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                   <td>
                                       <input type="submit"  value="Guardar" class="boton" />
                                   </td>
                           </form> 
                               </tr>
                               <%
                                }%>
                               </table>
                          </div>
                      </div>
                      <div id="boletasRifa" class="ventanas" style="display: none;">
                          <form action="AdminCrearVentaRifaBean" method="post">
                              <table>
                                  <tr><th>NUEVA BOLETA</th></tr>
                                  <tr>
                                      <td>
                                          <label>Rifa</label>
                                      </td>
                                      <td>
                                          <select name="rifa" id="rifa" required="true" onchange="rangoRifa()">
                                              <%for (Rifa elem : outDTO.getRifas()) {
                                                 if(elem.getIdEstado().getId() == 1){
                                                         int id = elem.getId();
                                                         String valor = elem.getNombre();
                                                         int rango = elem.getNumeroMaxRifa();
                                                          %>
                                                             <option value="<%=id+","+rango%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                }
                                                     %>
                                               </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Usuario</label>
                                      </td>
                                      <td>
                                          <select name="usuario" id="usuario" required="true">
                                              <%for (Usuario elem : outDTO.getUsuarios()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getNombre();
                                                         if(id != 1){
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                          }
                                                        } 
                                                     %>
                                               </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Premio a Participar</label>
                                      </td>
                                      <td>
                                          <select id="tipoPermio" name="tipoPermio" required="true">
                                              <option value="1">Premio Principal</option>
                                              <option value="2">Segundo Premio</option>
                                              <option value="3">Tercer Premio</option>
                                              <option value="4">Cuarto Premio</option>
                                              <option value="5">Quinto Premio</option>
                                          </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Numero para Participar</label>
                                      </td>
                                      <td>
                                          <input type="number" id="rangoData" name="rangoData" onchange="rangoRifa()" required="true"/>
                                      </td>
                                  </tr>
                                   <tr>
                                      <td>
                                          <label>Estado</label>
                                      </td>
                                      <td>
                                          <select name="estado" id="estado" required="true">
                                              <%for (Estados elem : outDTO.getEstados()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td></td>
                                      <td>
                                          <br>
                                          <input type="submit"  value="Guardar" class="boton" />
                                      </td>
                                  </tr>
                                      
                              </table>
                          </form>
                          <hr>
                          <div style="height: 400px; overflow: scroll;">
                              <table>
                                  <%
                                    if(outDTO.getVentaRifas().size() > 0){
                                  %>
                                  <tr><th>REGISTRO BOLETAS RIFA</th></tr>
                               <tr>
                                   <td>
                                       <label>Nombre Rifa</label>
                                   </td>
                                   <td>
                                       <label>Usuario</label>
                                   </td>
                                   <td>
                                       <label>Valor Apostado</label>
                                   </td>
                                   <td>
                                       <label>Resultado</label>
                                   </td>
                                   <td>
                                       <label>Premio</label>
                                   </td>
                                   <td>
                                       <label>Valor Jugado</label>
                                   </td>
                                   <td>
                                       <label>Estado</label>
                                   </td>
                                   <td></td>
                               </tr>
                               <%
                                }
                                for (VentaRifas rifa : outDTO.getVentaRifas()) {
                                    int idVentaRifa = rifa.getId();
                                    int idRifa  = rifa.getIdRifa().getId();
                                    int idUsuario = rifa.getIdUsuario().getId();
                                    int valorRifa = rifa.getValorRifa();
                                    String resultado = rifa.getResultado() ;
                                    String premio = rifa.getTipoPremio();
                                    int marcadorJuego = rifa.getMarcadorJuego();
                                    int estado = rifa.getIdEstado().getId();
                                    
                                %>
                               <tr>
                                  <form action="AdminModificarVentaRifaBean" method="post">
                                   <input type="hidden" value="<%=idVentaRifa%>" name="idVentaRifa"/>
                                   <td>
                                           <select name="rifaM" id="rifaM<%=idVentaRifa%>" required="true" >
                                                <%for (Rifa elem : outDTO.getRifas() ) {
                                                        int id = elem.getId();
                                                        String valor = elem.getNombre();
                                                        int rango = elem.getNumeroMaxRifa();
                                                        if(idRifa == id){
                                                        %>
                                                            <option value="<%=id+","+rango%>" selected="true" ><%=valor%></option>
                                                        <%
                                                        }else{  
                                                         %>
                                                            <option value="<%=id+","+rango%>"><%=valor%></option>
                                                         <%
                                                        }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                       <td>
                                           <select name="usuarioM" id="usuarioM" required="true">
                                                <%for (Usuario elem : outDTO.getUsuarios() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getNombre();
                                                    if(id != 1){
                                                        if(idUsuario == id){
                                                        %>
                                                            <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                        <%
                                                        }else{  
                                                         %>
                                                            <option value="<%=id%>"><%=valor%></option>
                                                         <%
                                                        }
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                   <td>
                                       <input type="number" value="<%=valorRifa%>" id="valorRifaM" name="valorRifaM" required="true" />
                                   </td>
                                   <td>
                                       <input type="text" value="<%=resultado%>" id="resultadoM" name="resultadoM" required="true" readonly="true"/>
                                   </td>
                                   <td>
                                          <select id="tipoPermio" name="tipoPermio" required="true">
                                              <%
                                                  String p1="";
                                                  String p2="";
                                                  String p3="";
                                                  String p4="";
                                                  String p5="";
                                                  if(Integer.parseInt(premio) == 1){
                                                      p1 ="selected='true'";
                                                  }else if(Integer.parseInt(premio) == 2){
                                                      p2 ="selected='true'";
                                                  }else if(Integer.parseInt(premio) == 3){
                                                      p3 ="selected='true'";
                                                  }else if(Integer.parseInt(premio) == 4){
                                                      p4 ="selected='true'";
                                                  }else if(Integer.parseInt(premio) == 5){
                                                      p5 ="selected='true'";
                                                  }
                                              %>
                                              <option value="1" <%=p1%>>Premio Principal</option>
                                              <option value="2" <%=p2%>>Segundo Premio</option>
                                              <option value="3" <%=p3%>>Tercer Premio</option>
                                              <option value="4" <%=p4%>>Cuarto Premio</option>
                                              <option value="5" <%=p5%>>Quinto Premio</option>
                                          </select>
                                      </td>
                                      <td>
                                       <input type="text" value="<%=marcadorJuego%>" id="marcadorJuego" name="marcadorJuego" required="true"/>
                                      </td>
                                      <td>
                                          
                                           <select name="estadoM" id="estadoM" required="true">
                                                <%for (Estados elem : outDTO.getEstados() ) {
                                                    int id = elem.getId();
                                                    String valor = elem.getDescripcion();
                                                    if(estado == id){
                                                    %>
                                                        <option value="<%=id%>" selected="true" ><%=valor%></option>
                                                    <%
                                                    }else{
                                                     %>
                                                        <option value="<%=id%>"><%=valor%></option>
                                                     <%
                                                    }
                                                  }
                                                %>
                                            </select>
                                       </td>
                                   <td>
                                       <input type="submit"  value="Guardar" class="boton" />
                                   </td>
                           </form> 
                               </tr>
                               <%
                                }%>
                               </table>
                          </div>
                      </div>
                               <div id="reportes"  class="ventanas" style="display: none;">
                                   <table>
                                       <tr><th>REPORTES GANADORES</th></tr>
                                       <TR>
                                           <TD>
                                               <label>Mostrar reportes ganadores</label>
                                           </TD>
                                           <td>
                                               <select id="ganadoresF" name="ganadoresF">
                                                   <option value="true">Activar</option>
                                                   <option value="false">Desactivar</option>
                                               </select>
                                           </td>
                                       </TR>
                                       <tr>
                                           <td></td>
                                           <td>
                                               <input type="button" class="boton" value="Guardar" onclick="gestionarFormulario('AdminGestionarGanadoresBean','ganadoresF');"AdminGestionarGanadoresBean/>
                                           </td>
                                       </tr>
                                       
                                   </table>
                                   <br>
                                   <table>
                                       <tr><th>FILTROS</th></tr>
                                <tr>
                                    <td>
                                        <select id="filtros">
                                            <option value="rifa">Rifas</option>
                                            <option value="apuesta">Apuestas</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="button" class="boton" value="Consultar" onclick="filtrosAdmin()"/>
                                    </td>
                                </tr>
                            </table>
                            <div id="apuestaFiltro" style="display: none;">
                                <table>
                                    <tr>
                                        <td>
                                            <label>Deporte</label>
                                        </td>
                                        <td>
                                            <label>Campeonato</label>
                                        </td>
                                        <td>
                                            <label>Fecha</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="deporteR" id="deporteR" >
                                                <option></option>
                                                  <%for (Deporte elem : outDTO.getDeportes()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="campeonatoR" id="campeonatoR" >
                                                <option></option>
                                                  <%for (Campeonato elem : outDTO.getCampeonatos()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getDescripcion();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                            </select>
                                        </td>
                                        <td>
                                                   <input type="text" id="fechaR" name="fechaR" class="datepicker"/>
                                        </td>
                                        
                                    </tr>
                                </table>
                            </div>
                        <div id="ganadores">

                        </div>
                               </div>
                </div>
            </section>
      
    </div>
    </body>
    
        
</html>
