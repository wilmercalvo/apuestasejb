<%-- 
    Document   : index
    Created on : 23/02/2019, 09:46:08 PM
    Author     : User
--%>

<%@page import="com.uniminuto.util.UtilDTO"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.uniminuto.entity.Parametros"%>
<%@page import="com.uniminuto.entity.Partido"%>
<%@page import="com.uniminuto.entity.Campeonato"%>
<%@page import="java.util.List"%>
<%@page import="com.uniminuto.entity.Deporte"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>..::UNIAPUESTAS::..</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
        <link rel="stylesheet" type="text/css" href="css/slider.css" media="all" />
        <!-- jQuery -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <!-- FlexSlider -->
        <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
        <script type="text/javascript" src="js/Slider.js"></script>
        <script type="text/javascript" src="js/home.js"></script>
        
        <LINK href="css/Menu.css" rel="stylesheet" type="text/css">
        
        
    </head>
    
    <body onload="cargarSlider(); setInterval('gestionarJuegos()',60000);">
        <div id="div-contenedor">
        <div class="slider_container">
		<div class="flexslider">
	      <ul class="slides">
	    	<li>
	    		<a href="#"><img src="images/slider/slide1.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Party Hairstyles</h2><p>Find the perfect hairstyle a la Lauren Conrad for every type of festive fête.</p></div>-->
                </div>
	    	</li>
	    	<li>
	    		<a href="#"><img src="images/slider/slide2.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Beautiful Hairstyle</h2><p>The latest hairstyles and instructions on how to create them here. Total Beauty has your complete hairstyles guide</p></div>-->
                </div>
	    	</li>
	    	<li>
	    		<a href="#"><img src="images/slider/slide3.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Party Dresses</h2><p>If you are looking for something a little special for your big night out, check out Rare London's collection of stunning party dresses</p></div>-->
                </div>
	    	</li>
	    	<li >
	    		<a href="#"><img src="images/slider/slide4.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Bodycon Dresses</h2><p>The bodycon dress is a key silhouette for this season's party girl; from sleek colour-block panelling to geometric prints the bodycon.</p></div>-->
                </div>
	    	</li>
	    </ul>
	  </div>
	  </div>
            
        <header class="page-header1">
        <div class="center-contents1">
          <nav class="page-nav1">
            <ul id="menu11">
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('homeCatalogo');" >Home</a></li>
              <div style="display: none;" id="ganadoresId">
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('filtro');" >Ganadores</a></li>
              </div>
              <li class="items1" style="float:right"><a class="active1" href="#" onClick="gestionVentana('registrar');">Registar</a></li>
              <li class="items1" style="float:right"><a class="active1" href="#" onClick="gestionVentana('loginDiv');">Login</a></li>
            </ul>
          </nav>
        </div>
            </header>
                <div id="principal">
                    <div id="homeCatalogo" class="ventanas">
                        <MARQUEE BGCOLOR="F0F9EA" id="textoResultado"> </MARQUEE>
                        <MARQUEE BGCOLOR="#ECEFEA" id="textoResultadoRifa"> </MARQUEE>
                        <div id="catalogo">
                         
                        </div>
                    </div>
                    
                    <br>
                    <div id="loginDiv" class="ventanas" style="display: none;">
                        <form action="LoginBean" method="post">
                            <table>
                                <tr>
                                    <td>
                                        <label class="texto">Usuario:</label>    
                                    </td>
                                    <td>
                                        <input type="text" name="user" id="user" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="texto">Contraseña:</label>    
                                    </td>
                                    <td>
                                        <input type="password" name="pass" id="pass" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input class="boton" type="submit" value="Ingresar"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div id="registrar" class="ventanas" style="display: none;">
                        <form action="AdminRegistrarUsuarioBean" method="post">
                            <table>
                                <tr>
                                    <td>
                                        <label>Nombre</label>
                                    </td>
                                    <td>
                                        <input type="text" id="nombre" name="nombre" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Documento</label>
                                    </td>
                                    <td>
                                        <input type="number" id="numeroD" name="numeroD" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>UserName</label>
                                    </td>
                                    <td>
                                        <input type="text" id="username" name="username" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Password</label>
                                    </td>
                                    <td>
                                        <input type="password" id="password" name="password" required="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <br>
                                        <input type="submit" class="boton" value="Registrar"/>
                                    </td>
                                </tr>
                                
                            </table>
                        </form>
                    </div>
                    <div id="filtro" class="ventanas" style="display: none;">
                            <table>
                                <tr>
                                    <td>
                                        <select id="filtros">
                                            <option value="rifa">Rifas</option>
                                            <option value="apuesta">Apuestas</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="button" class="boton" value="Consultar" onclick="filtros()"/>
                                    </td>
                                </tr>
                            </table>
                            <div id="apuestaFiltro" style="display: none;">
                                <table>
                                    <tr>
                                        <td>
                                            <label>Deporte</label>
                                        </td>
                                        <td>
                                            <label>Campeonato</label>
                                        </td>
                                        <td>
                                            <label>Partido</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="idDivCamp">
                                                
                                            </div>
                                        </td>
                                        <td>
                                            <div id="idDivDepor">
                                                
                                            </div>
                                        </td>
                                        <td>
                                            <div id="idDivPart">
                                                
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        <div id="ganadores">

                        </div>    
                    </div>
                    
    </div>
    </body>
    
</html>
