var contadorSlider = 1;
var contadorFlag = 1;

function login(){
$(document).ready(function () {
    $("#loginDiv").fadeIn("slow");
 });
}

function gestionVentana(venta){
$(document).ready(function () {
    $(".ventanas").fadeOut("fast");
    $("#"+venta).fadeIn("slow");
    
 });
}

$(function(){
  $(".datepicker").datetimepicker({
    format: "d/m/Y H:i"
  });
  $.datetimepicker.setLocale('es');
})

function gestionarFormulario(servlet,dato){
    $(document).ready(function () {
        var datos = dato.split(",");
        var resp='{';
        for(var i = 0; i < datos.length; i++){
            resp = resp + '"'+datos[i]+'":';
            var valor = document.getElementById(datos[i]).value;
            if(valor === ""){
                alert("Por favor Ingrese los campos requeridos");
                return;
            }else{
                resp = resp +'"'+ valor.toUpperCase() +'"';
                if((i + 1) < datos.length ){
                    resp = resp +",";
                }
            }
        }
        resp=resp+'}';
        $.ajax({
          async: false,
          cache: false,
          url: servlet,
          type: 'POST',
          data:JSON.parse(resp),
          beforeSend: function (xhr) {
        
          },
          success: function (response) {
              try
            {
              alert(response);
              location.reload();
            } catch (e)
            {
                alert(e);
            }
        
          },
          error: function (jqXHR, textStatus, errorThrown) {
        
          }
      });
     });
 }
 
 
 function gestionarJuegos(){
    $(document).ready(function () {
        $.ajax({
          async: false,
          cache: false,
          url: "ApuestaBean",
          type: 'GET',
          data:{},
          beforeSend: function (xhr) {
          },
          success: function (response) {
              location.reload();
          },
          error: function (jqXHR, textStatus, errorThrown) {
        
          }
      });
     });
 }
 
 function cargaInicial(){
    $(document).ready(function () {
        $.ajax({
          async: false,
          cache: false,
          url: "CatalogoBean",
          type: 'POST',
          data:{},
          beforeSend: function (xhr) {
          },
          success: function (response) {
              try{
                   var texto ="flex"; 
                   var objResponse = JSON.parse(response);
                   var data = objResponse.responseApuesta;
                   var dataRifa = objResponse.responseRifa;
                   var numeroApuesta = objResponse.tamApuesta;
                   var numeroRifa = objResponse.tamRifa;
                   var resultados = objResponse.resultados;
                   var resultadosRifa = objResponse.resultadosRifas;
                   var result ="";
                   var resultRifa ="";
                   for (var q = 0; q < resultados.length; q++)
                   {
                       result += resultados[q].resultado +" - ";
                   }
                   $("#textoResultado").html("");
                   $("#textoResultado").append(result);
                   
                   for (var w = 0; w < resultadosRifa.length; w++)
                   {
                       resultRifa += resultadosRifa[w].resultado +" - " ;
                   }
                   $("#textoResultadoRifa").html("");
                   $("#textoResultadoRifa").append(resultRifa);
                   var datos ="";
                   datos +="<ul>";
                   datos +="<li>";
                   datos +="<div id="+texto.concat(contadorSlider)+">";
                   var conta = 0; 
                   var contaRifa = 0; 
                   for (var i = 0; i < numeroApuesta; i++)
                   {
                       if(conta === 6){
                           conta = 0;
                           contadorSlider ++;
                           datos +="</div>";
                           datos +="</li>";
                           datos +="<li>";
                           datos +="<div id='"+texto.concat(contadorSlider)+"' style='display: none;'>";
                      }
                       datos +="<div id='catalogoVista'><h2 id='titCata'>"+data[i].nombre+"</h2><hr><h3 id='subCata'>"+data[i].descripcion+"</h3><p id='contCata'>"+data[i].equiLocal+" vs "+data[i].equiVisit+"<br><label  class='titulos'>Valor Apuesta: $ </label>"+data[i].valor+"<br><label  class='titulos'>Fecha: $ </label>"+data[i].fecha+"</p></div>";
                       conta ++;
                   }
                   if(contadorSlider > 0){
                      contadorSlider ++;
                      datos +="</div>";
                      datos +="</li>";
                      datos +="<li>";
                      datos +="<div id='"+texto.concat(contadorSlider)+"' style='display: none;'>"; 
                   }
                   for (var j = 0; j < numeroRifa; j++)
                   {
                       if(contaRifa === 2){
                           contaRifa = 0;
                           contadorSlider ++;
                           datos +="</div>";
                           datos +="</li>";
                           datos +="<li>";
                           datos +="<div id='"+texto.concat(contadorSlider)+"' style='display: none;'>";
                      }
                       datos +="<div id='catalogoVistaRifa'><h2 id='titCata'>"+dataRifa[j].nombre+"</h2><hr><h3 id='subCata'>"+dataRifa[j].descripcion+"</h3><p id='contCata'><label  class='titulos'>Primer Premio: </label>"+dataRifa[j].p1+"<br><label  class='titulos'>Segundo Premio: </label>"+dataRifa[j].p2+"<br><label  class='titulos'>Tercer Premio: </label>"+dataRifa[j].p3+"<br><label  class='titulos'>Cuarto Premio: </label>"+dataRifa[j].p4+"<br><label  class='titulos'>Quinto Premio: </label>"+dataRifa[j].p5+"<br><label  class='titulos'>Cupos Disponibles: </label>"+dataRifa[j].cupos+"<br><label  class='titulos'>Valor: $ </label>"+dataRifa[j].valor+"<br><label  class='titulos'>Fecha: </label>"+dataRifa[j].fecha+"</p></div>";
                       contaRifa ++;
                   }
                   datos +="</div>";
                   datos +="</li>";
                   datos +="</ul>";
                   $("#catalogo").html("");
                   $("#catalogo").append(datos);
              }catch(e){
                  alert("Servicio no disponible " + e);
              }
          },
          error: function (jqXHR, textStatus, errorThrown) {
        
          }
      });
      //lista campeonato, deporte, partido y parametros
      $.ajax({
          async: false,
          cache: false,
          url: "CargarListas",
          type: 'GET',
          data:{},
          beforeSend: function (xhr) {
          },
          success: function (response) {
              try{
                   var objResponse = JSON.parse(response);
                   var campeonato = objResponse.campeonato;
                   var deporte = objResponse.deporte;
                   var partido = objResponse.partido;
                   var parametro = objResponse.parametro;
                   
                   var camR="";
                   var depR="";
                   var partR="";
                   var paraR="";
                  depR += "<select name='deporte' id='deporte' >"; 
                  for (var d = 0; d < deporte.length; d++)
                   {
                       depR += "<option value='"+deporte[d].id+"' >"+deporte[d].valor+"</option>" ;
                   }
                   depR +="</select>";
                   
                  camR += "<select name='campeonato' id='campeonato' >"; 
                  for (var c = 0; c < campeonato.length; c++)
                   {
                       camR += "<option value='"+campeonato[c].id+"' >"+campeonato[c].valor+"</option>" ;
                   }
                   camR +="</select>";
                   
                  partR += "<select name='partido' id='partido' >"; 
                  for (var p = 0; p < partido.length; p++)
                   {
                       partR += "<option value='"+partido[p].id+"' >"+partido[p].valor+"</option>" ;
                   }
                   partR +="</select>";
                   
                   var flagPara = parametro[0].valor;
                   if(flagPara === "FALSE"){
                       $("#ganadoresId").css("display","none");
                   }else{
                       $("#ganadoresId").css("display","block");
                   }
                   
                  $("#idDivCamp").html("");
                  $("#idDivCamp").append(camR);
                  
                  $("#idDivDepor").html("");
                  $("#idDivDepor").append(depR);
                  
                  $("#idDivPart").html("");
                  $("#idDivPart").append(partR);
                   
              }catch(e){
                  alert("Servicio no disponible " + e);
              }
          },
          error: function (jqXHR, textStatus, errorThrown) {
        
          }
      });
      
     });
 }
 
 function sliderCatalogo(){
$(document).ready(function () {
    var texto ="#flex";
    if(contadorSlider > contadorFlag){
        $(texto.concat(contadorFlag)).fadeOut("fast");
        $(texto.concat(parseInt(contadorFlag)+ parseInt(1))).fadeIn("slow");
        contadorFlag ++;
    }else{
        contadorFlag = 0;
        $(texto.concat(contadorSlider)).fadeOut("fast");
        $(texto.concat(parseInt(contadorFlag) + parseInt(1))).fadeIn("slow");
        contadorFlag ++;
    }
    
 });
}

function cargarSlider(){
    cargaInicial();
    setInterval('sliderCatalogo()',5000);
}

function rangoRifa(){
   var flagRangoRifa = document.getElementById("rifa").value;
   var datos = flagRangoRifa.split(",");
   var rango = document.getElementById("rangoData").value;
   
   if(parseInt(datos[1]) < parseInt(rango) || parseInt(rango) <= 0){
       alert("El valor del numero para participar, \ndebe ser menor o igual a " + datos[1] +"\nNo puede ser 0 (cero) ");
       return false;
   }
}


function logout(){
    $(document).ready(function () {
        $.ajax({
          async: false,
          cache: false,
          url: "LogOutBean",
          type: 'POST',
          data:{},
          beforeSend: function (xhr) {
          },
          success: function (response) {
              window.location = '/ApuestasApplication-war/'+response;
          },
          error: function (jqXHR, textStatus, errorThrown) {
        
          }
      });
     });
}

 function filtros(){
    $(document).ready(function () {
        var filtro;
        if($("#filtros").val() === "apuesta"){
            filtro = "2";
        }else{
            filtro = "1";
        }
        $.ajax({
          async: false,
          cache: false,
          url: "GanadoresBean",
          type: 'POST',
          data:{type:filtro, deporte: $("#deporte").val(), campeonato: $("#campeonato").val(), partido: $("#partido").val()},
          beforeSend: function (xhr) {
          },
          success: function (response) {
              try{
                   var texto ="flex"; 
                   var objResponse = JSON.parse(response);
                   var data = objResponse.ganadores;
                   
                   var datos ="";
                   datos +="<table class='reportesG'>"; 
                   if(data.length > 0){
                        datos +="<tr>";
                        datos +="<td>";
                        datos +="<label>Nombre</label>";
                        datos +="</td>";
                        datos +="<td>";
                        datos +="<label>Identificacion</label>";
                        datos +="</td>";
                        datos +="<td>";
                        datos +="<label>Evento</label>";
                        datos +="</td>";
                        datos +="</tr>";
                   }
                   for (var i = 0; i < data.length; i++)
                   {
                       datos +="<tr><td>"+data[i].nombre+"</td><td>"+data[i].cc+"</td><td>"+data[i].evento+"</td></tr>";
                   }
                   
                   datos +="</table>";
                   $("#ganadores").html("");
                   $("#ganadores").append(datos);
              }catch(e){
                  alert("Servicio no disponible " + e);
              }
          },
          error: function (jqXHR, textStatus, errorThrown) {
        
          }
      });
     });
 }
 
 $(document).ready(function () {
    $("#filtros").change(function (){
        $("#ganadores").html("");
        if($("#filtros").val() === "apuesta"){
            $("#apuestaFiltro").css("display","block");
        }else{
            $("#apuestaFiltro").css("display","none");
        }
    });
    
 });
 
 
 function filtrosAdmin(){
    $(document).ready(function () {
        var filtro;
        if($("#filtros").val() === "apuesta"){
            filtro = "2";
        }else{
            filtro = "1";
        }
        $.ajax({
          async: false,
          cache: false,
          url: "ReportesBean",
          type: 'POST',
          data:{type:filtro, deporte: $("#deporteR").val(), campeonato: $("#campeonatoR").val(), fecha: $("#fechaR").val()},
          beforeSend: function (xhr) {
          },
          success: function (response) {
              try{
                   var objResponse = JSON.parse(response);
                   var data = objResponse.reporte;
                   
                   var datos ="";
                   datos +="<table class='reportesG'>"; 
                   if(data.length > 0){
                        datos +="<tr>";
                        datos +="<td>";
                        datos +="<label>Nombre</label>";
                        datos +="</td>";
                        datos +="<td>";
                        datos +="<label>Identificacion</label>";
                        datos +="</td>";
                        datos +="<td>";
                        datos +="<label>Evento</label>";
                        datos +="</td>";
                        datos +="<td>";
                        datos +="<label>Resultado</label>";
                        datos +="</td>";
                        datos +="<td>";
                        datos +="<label>Valor Apostado</label>";
                        datos +="</td>"
                        datos +="<td>";
                        datos +="<label>Resultado Evento</label>";
                        datos +="</td>";
                        datos +="<td>";
                        datos +="<label>Fecha</label>";
                        datos +="</td>";
                        datos +="<td>";
                        datos +="<label>Valor</label>";
                        datos +="</td>";
                        datos +="<td>";
                        datos +="<label>Estado</label>";
                        datos +="</td>";
                        datos +="</tr>";
                   }
                   for (var i = 0; i < data.length; i++)
                   {
                       if(data[i].resultado === "GANADO"){
                            datos +="<tr><td>"+data[i].nombre+"</td><td>"+data[i].cc+"</td><td>"+data[i].evento+"</td><td style='background: #6BED37;'>"+data[i].resultado+"</td><td>"+data[i].valorApostado+"</td><td>"+data[i].resultadoEvento+"</td><td>"+data[i].fecha+"</td><td>"+data[i].valor+"</td><td>"+data[i].estado+"</td></tr>";
                       }else if(data[i].resultado === "PERDIDO"){
                           datos +="<tr><td>"+data[i].nombre+"</td><td>"+data[i].cc+"</td><td>"+data[i].evento+"</td><td style='background: #F88757;'>"+data[i].resultado+"</td><td>"+data[i].valorApostado+"</td><td>"+data[i].resultadoEvento+"</td><td>"+data[i].fecha+"</td><td>"+data[i].valor+"</td><td>"+data[i].estado+"</td></tr>";
                        }
                  }
                   
                   datos +="</table>";
                   
                   $("#ganadores").html("");
                   $("#ganadores").append(datos);
              }catch(e){
                  alert("Servicio no disponible " + e);
              }
          },
          error: function (jqXHR, textStatus, errorThrown) {
        
          }
      });
     });
 }