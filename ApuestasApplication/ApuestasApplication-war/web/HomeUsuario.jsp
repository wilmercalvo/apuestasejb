<%-- 
    Document   : index
    Created on : 23/02/2019, 09:46:08 PM
    Author     : User
--%>

<%@page import="com.uniminuto.util.SesionDTO"%>
<%@page import="com.uniminuto.entity.VentaRifas"%>
<%@page import="com.uniminuto.entity.Rifa"%>
<%@page import="com.uniminuto.entity.Premios"%>
<%@page import="com.uniminuto.entity.Marcador"%>
<%@page import="com.uniminuto.entity.VentaApuestas"%>
<%@page import="com.uniminuto.entity.Apuesta"%>
<%@page import="com.uniminuto.entity.Partido"%>
<%@page import="com.uniminuto.entity.EquipoJugador"%>
<%@page import="com.uniminuto.entity.Campeonato"%>
<%@page import="com.uniminuto.entity.Deporte"%>
<%@page import="com.uniminuto.entity.Usuario"%>
<%@page import="com.uniminuto.entity.Estados"%>
<%@page import="com.uniminuto.entity.Perfil"%>
<%@page import="com.uniminuto.util.UtilDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>..::UNIAPUESTAS::..</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
        <link rel="stylesheet" type="text/css" href="css/slider.css" media="all" />
        <!-- jQuery -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <!-- FlexSlider -->
        <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
        <script type="text/javascript" src="js/Slider.js"></script>
        <script type="text/javascript" src="js/home.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.full.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css" rel="stylesheet"/>

        <LINK href="css/Menu.css" rel="stylesheet" type="text/css">
        <%HttpSession sesion = request.getSession();
          UtilDTO outDTO = (UtilDTO) sesion.getAttribute("listas");
          SesionDTO sDTO = (SesionDTO) sesion.getAttribute("sesionDTO");
          int idUsuario = sDTO.getIdUsuario();
          String nombre = sDTO.getNombre();
          int cupo = sDTO.getCupo();
          for (Usuario elem : outDTO.getUsuarios()) {
                  if(elem.getId() ==  idUsuario){
                      cupo = elem.getCupoDinero();
                  }
              }
          
        %>
    </head>
    
    <body onLoad="setInterval('gestionarJuegos()',60000);">
	<div id="div-contenedor">
        <div class="slider_container">
		<div class="flexslider">
	      <ul class="slides">
	    	<li>
	    		<a href="#"><img src="images/slider/slide1.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Party Hairstyles</h2><p>Find the perfect hairstyle a la Lauren Conrad for every type of festive fête.</p></div>-->
                </div>
	    	</li>
	    	<li>
	    		<a href="#"><img src="images/slider/slide2.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Beautiful Hairstyle</h2><p>The latest hairstyles and instructions on how to create them here. Total Beauty has your complete hairstyles guide</p></div>-->
                </div>
	    	</li>
	    	<li>
	    		<a href="#"><img src="images/slider/slide3.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Party Dresses</h2><p>If you are looking for something a little special for your big night out, check out Rare London's collection of stunning party dresses</p></div>-->
                </div>
	    	</li>
	    	<li >
	    		<a href="#"><img src="images/slider/slide4.jpg" alt="" title=""/></a>
	    		<div class="flex-caption">
                     <!--<div class="caption_title_line"><h2>Bodycon Dresses</h2><p>The bodycon dress is a key silhouette for this season's party girl; from sleek colour-block panelling to geometric prints the bodycon.</p></div>-->
                </div>
	    	</li>
	    </ul>
	  </div>
	  </div>
       

        <header class="page-header1">
        <div class="center-contents1">
          <nav class="page-nav1">
            <ul id="menu11">
                
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('boletasApuesta');" >Boletas Apuesta</a></li>
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('boletasRifa');" >Boletas Rifa</a></li>
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('rifaH');" >Historial Rifa</a></li>
              <li class="items1"><a class="links1" href="#" onClick="gestionVentana('apuestaH');" >Historial Apuesta</a></li>
              <li class="items1" style="float:right"><a class="active1" href="#" onClick="logout();">LogOut</a></li>
            </ul>
          </nav>
        </div>
        </header>    
            <section>
                <div id="principal">
                    <div id="datos"> 
                        <p id="datoTitulo" >Bienvenid@ <%=nombre%></p>
                        <p id="datoCupo">Cupo disponible $ <label style="background: #99F857;"><%=cupo%></label></p>
                        <br>
                    </div>
                       <div id="boletasApuesta" class="ventanas" style="display: none;">
                          <br>
                          <div>
                              <form action="AdminCrearVentaApuestaBean" method="post">
                                  <table>
                                      <tr><th>NUEVA APUESTA</th></tr>
                                      <tr>
                                          <td>
                                              <label>Apuesta</label>
                                          </td>
                                          <td>
                                              <select name="ventaApuestaApuesta" id="ventaApuestaApuesta" required="true">
                                                       <%for (Apuesta elem : outDTO.getApuestas()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getNombre() +" - "+elem.getIdPartido().getIdEquipoLocal().getNombre() +" vs "+elem.getIdPartido().getIdEquipoVisitante().getNombre();;
                                                         if(elem.getIdEstado().getId() == 1 && elem.getIdPartido().getIdEstado().getId() == 1){
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                         }
                                                       }
                                                     %>
                                               </select>
                                               <input type="hidden" name="ventaUsuarioApuesta" id="ventaUsuarioApuesta" value="<%=idUsuario%>"/>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <label>Valor Apostado</label>
                                          </td>
                                          <td>
                                              <input type="number" id="ventaValorApuesta" name="ventaValorApuesta" required="true"/>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <label>Marcador Apostado</label>
                                          </td>
                                          <td>
                                              <select name="marcadorApuesta" id="marcadorApuesta" required="true">
                                                  <%for (Marcador elem : outDTO.getMarcadores()) {
                                                         int id = elem.getId();
                                                         String valor = elem.getValor();
                                                          %>
                                                             <option value="<%=id%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                     %>
                                               </select>
                                          </td>
                                      </tr>
                                      
                                      <tr>
                                          <td></td>
                                          
                                            <td>
                                                <input type="submit"  value="Guardar" class="boton" />
                                            </td>
                                          
                                      </tr>
                                  </table>
                              </form>
                          </div>               
                      </div>
                      <div id="boletasRifa" class="ventanas" style="display: none;">
                          <form action="AdminCrearVentaRifaBean" method="post">
                              <table>
                                  <tr><th>NUEVA RIFA</th></tr>
                                  <tr>
                                      <td>
                                          <label>Rifa</label>
                                      </td>
                                      <td>
                                          <select name="rifa" id="rifa" required="true" onchange="rangoRifa()">
                                              <%for (Rifa elem : outDTO.getRifas()) {
                                                 if(elem.getIdEstado().getId() == 1){
                                                         int id = elem.getId();
                                                         String valor = elem.getNombre();
                                                         int rango = elem.getNumeroMaxRifa();
                                                          %>
                                                             <option value="<%=id+","+rango%>" ><%=valor%></option>
                                                             <%
                                                       }
                                                }
                                                     %>
                                               </select>
                                               <input type="hidden" name="usuario" id="usuario" value="<%=idUsuario%>"/>
                                      </td>
                                  </tr>
                                  
                                  <tr>
                                      <td>
                                          <label>Premio a Participar</label>
                                      </td>
                                      <td>
                                          <select id="tipoPermio" name="tipoPermio" required="true">
                                              <option value="1">Premio Principal</option>
                                              <option value="2">Segundo Premio</option>
                                              <option value="3">Tercer Premio</option>
                                              <option value="4">Cuarto Premio</option>
                                              <option value="5">Quinto Premio</option>
                                          </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <label>Numero para Participar</label>
                                      </td>
                                      <td>
                                          <input type="number" id="rangoData" name="rangoData" onchange="rangoRifa()" required="true"/>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td></td>
                                      <td>
                                          <br>
                                          <input type="submit"  value="Guardar" class="boton" />
                                      </td>
                                  </tr>
                                      
                              </table>
                          </form>
                      </div>
 
                                      <div id="rifaH" style="height: 800px; overflow: scroll; display: none;" class="ventanas">
                                                                               <%
                                                   boolean flagRifa = false;
                                                   for (VentaRifas elem : outDTO.getVentaRifas()) {
                                                         if(elem.getIdUsuario().getId() == idUsuario){
                                                             flagRifa = true;
                                                         }
                                                    }
                                                   if(!flagRifa){
                                                       %>
                                                       <br>
                                                       <h1 style="color: #B41B0B">No tiene Registro</h1>
                                                       <%
                                                   }else{
                                                   %>
                                          <table class='reportesG'>
                                              <tr><th>HISTORIAL RIFA</th></tr>
                                              <tr>
                                                  <td>
                                                      <label>Nombre Rifa</label>
                                                  </td>
                                                  <td>
                                                      <label>Resultado</label>
                                                  </td>
                                                  <td>
                                                      <label>Valor</label>
                                                  </td>
                                                  <td>
                                                      <label>Numero Apostado</label>
                                                  </td>
                                                  <td>
                                                      <label>Numero Ganador</label>
                                                  </td>
                                                  <td>
                                                      <label>Fecha Jugado</label>
                                                  </td>
                                                  <TD></TD>
                                              </tr>
                                              <tr>
                                                  <%
                                                      for (VentaRifas elem : outDTO.getVentaRifas()) {
                                                              String nombreRifa = elem.getIdRifa().getNombre();
                                                              int valor = elem.getValorRifa();
                                                              String  resultado = elem.getResultado();
                                                              int numApostado = elem.getMarcadorJuego();
                                                              String numGanador = elem.getIdRifa().getResultado();
                                                              String fecha = elem.getIdRifa().getFechaRifa();
                                                              
                                                                if(idUsuario == elem.getIdUsuario().getId()){
                                                              %>
                                              <tr>
                                                              <td>
                                                                  <label><%=nombreRifa%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=resultado%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=valor%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=numApostado%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=numGanador%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=fecha%></label>
                                                              </td>
                                              <tr>
                                                              <%
                                                          }
                                                        }
                                                  %>
                                              </tr>
                                          </table>
                                              <%}%>
                                      </div>
                                                   
                                                   
                                              <div id="apuestaH" style="height: 800px; overflow: scroll; display: none;" class="ventanas">
                                           <% 
                                                        boolean flagApuesta = false;
                                                   for (VentaApuestas elem : outDTO.getVentaApuestas()) {
                                                         if(elem.getIdUsuario().getId() == idUsuario){
                                                             flagApuesta = true;
                                                         }
                                                    }
                                                   if(!flagApuesta){
                                                       %>
                                                       <br>
                                                       <h1 style="color: #B41B0B">No tiene Registro</h1>
                                                       <%
                                                   }else{
                                                   %>
                                                  <table class='reportesG'>
                                                      <tr><th>HISTORIAL APUESTAS</th></tr>
                                              <tr>
                                                  <td>
                                                      <label>Nombre Apuesta</label>
                                                  </td>
                                                  <td>
                                                      <label>Resultado</label>
                                                  </td>
                                                  <td>
                                                      <label>Valor</label>
                                                  </td>
                                                  <td>
                                                      <label>Marcador Apostado</label>
                                                  </td>
                                                  <td>
                                                      <label>Marcador Ganador</label>
                                                  </td>
                                                  <td>
                                                      <label>Fecha Jugado</label>
                                                  </td>
                                                  <TD></TD>
                                              </tr>
                                              <tr>
                                                  <%
                                                      for (VentaApuestas elem : outDTO.getVentaApuestas()) {
                                                              String nombreA = elem.getIdApuesta().getNombre();
                                                              int valorA = elem.getValorApuesta();
                                                              String  resultado = elem.getResultado();
                                                              String marcadorApostado = elem.getMarcadorJuego().getValor();
                                                              int numGanador = elem.getIdApuesta().getIdPartido().getMarcadorJuego();
                                                              String marcadorGanador ="";
                                                              for (Marcador mar : outDTO.getMarcadores()) {
                                                                  if(numGanador == mar.getId()){
                                                                      marcadorGanador = mar.getValor();
                                                                  }
                                                              }
                                                              String fecha = elem.getIdApuesta().getIdPartido().getFechaPartido();
                                                              
                                                                if(idUsuario == elem.getIdUsuario().getId()){
                                                              %>
                                              <tr>
                                                              <td>
                                                                  <label><%=nombreA%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=resultado%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=valorA%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=marcadorApostado%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=marcadorGanador%></label>
                                                              </td>
                                                              <td>
                                                                  <label><%=fecha%></label>
                                                              </td>
                                              <tr>
                                                              <%
                                                          }
                                                        }
                                                  %>
                                              </tr>
                                          </table>
                                              <%
                                             } 
                                              %>
                                      </div>
                                              
                                              
                </div>
            </section>
      
    </div>
    </body>
    
        
</html>
