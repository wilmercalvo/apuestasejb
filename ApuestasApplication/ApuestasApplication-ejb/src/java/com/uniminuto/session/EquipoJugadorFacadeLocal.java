/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.session;

import com.uniminuto.entity.EquipoJugador;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author User
 */
@Local
public interface EquipoJugadorFacadeLocal {

    void create(EquipoJugador equipoJugador);

    void edit(EquipoJugador equipoJugador);

    void remove(EquipoJugador equipoJugador);

    EquipoJugador find(Object id);

    List<EquipoJugador> findAll();

    List<EquipoJugador> findRange(int[] range);

    int count();
    
    List<EquipoJugador> selectQueryId(String consulta,String id);
    
}
