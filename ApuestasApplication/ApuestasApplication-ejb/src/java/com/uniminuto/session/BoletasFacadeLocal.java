/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.session;

import com.uniminuto.entity.Boletas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author User
 */
@Local
public interface BoletasFacadeLocal {

    void create(Boletas boletas);

    void edit(Boletas boletas);

    void remove(Boletas boletas);

    Boletas find(Object id);

    List<Boletas> findAll();

    List<Boletas> findRange(int[] range);

    int count();
    
    List<Boletas> selectQueryId(String consulta,String id);
    
}
