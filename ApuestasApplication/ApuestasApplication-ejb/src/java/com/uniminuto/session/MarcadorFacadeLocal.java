/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.session;

import com.uniminuto.entity.Marcador;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author User
 */
@Local
public interface MarcadorFacadeLocal {

    void create(Marcador marcador);

    void edit(Marcador marcador);

    void remove(Marcador marcador);

    Marcador find(Object id);

    List<Marcador> findAll();

    List<Marcador> findRange(int[] range);

    int count();
    
    List<Marcador> selectQueryId(String consulta,String id);
    
}
