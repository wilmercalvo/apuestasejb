/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.session;

import com.uniminuto.entity.Usuario;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author User
 */
@Local
public interface UsuarioFacadeLocal {

    void create(Usuario usuario);

    void edit(Usuario usuario);

    void remove(Usuario usuario);

    Usuario find(Object id);

    List<Usuario> findAll();

    List<Usuario> findRange(int[] range);
    
    List<Usuario> selectQueryLogin(String query, String user, String pass);

    int count();
    
    List<Usuario> selectQueryId(String consulta,String id);
    
}
