/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.session;

import com.uniminuto.entity.Boletas;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author User
 */
@Stateless
public class BoletasFacade extends AbstractFacade<Boletas> implements BoletasFacadeLocal {

    @PersistenceContext(unitName = "ApuestasApplication-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BoletasFacade() {
        super(Boletas.class);
    }
    
}
