/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.session;

import com.uniminuto.entity.Premios;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author User
 */
@Local
public interface PremiosFacadeLocal {

    void create(Premios premios);

    void edit(Premios premios);

    void remove(Premios premios);

    Premios find(Object id);

    List<Premios> findAll();

    List<Premios> findRange(int[] range);

    int count();
    
    List<Premios> selectQueryId(String consulta,String id);
    
}
