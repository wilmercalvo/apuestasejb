/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.session;

import com.uniminuto.entity.VentaApuestas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author User
 */
@Local
public interface VentaApuestasFacadeLocal {

    void create(VentaApuestas ventaApuestas);

    void edit(VentaApuestas ventaApuestas);

    void remove(VentaApuestas ventaApuestas);

    VentaApuestas find(Object id);

    List<VentaApuestas> findAll();

    List<VentaApuestas> findRange(int[] range);

    int count();
    
    List<VentaApuestas> selectQueryId(String consulta,String id);
    
}
