/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.session;

import com.uniminuto.entity.VentaRifas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author User
 */
@Local
public interface VentaRifasFacadeLocal {

    void create(VentaRifas ventaRifas);

    void edit(VentaRifas ventaRifas);

    void remove(VentaRifas ventaRifas);

    VentaRifas find(Object id);

    List<VentaRifas> findAll();

    List<VentaRifas> findRange(int[] range);

    int count();
    
    List<VentaRifas> selectQueryId(String consulta,String id);
    
}
