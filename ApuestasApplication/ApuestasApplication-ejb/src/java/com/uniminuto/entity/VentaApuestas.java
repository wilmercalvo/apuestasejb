/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@Entity
@Table(name = "venta_apuestas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VentaApuestas.findAll", query = "SELECT v FROM VentaApuestas v")
    , @NamedQuery(name = "VentaApuestas.findById", query = "SELECT v FROM VentaApuestas v WHERE v.id = :id")
    , @NamedQuery(name = "VentaApuestas.findByValorApuesta", query = "SELECT v FROM VentaApuestas v WHERE v.valorApuesta = :valorApuesta")
    , @NamedQuery(name = "VentaApuestas.findByResultado", query = "SELECT v FROM VentaApuestas v WHERE v.resultado = :resultado")
    , @NamedQuery(name = "VentaApuestas.findByEstadoDescripcion", query = "SELECT v FROM VentaApuestas v WHERE v.estadoDescripcion = :estadoDescripcion")})
public class VentaApuestas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "VALOR_APUESTA")
    private Integer valorApuesta;
    @Size(max = 200)
    @Column(name = "RESULTADO")
    private String resultado;
    @Column(name = "ESTADO_DESCRIPCION")
    private Integer estadoDescripcion;
    @JoinColumn(name = "ID_APUESTA", referencedColumnName = "ID")
    @ManyToOne
    private Apuesta idApuesta;
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID")
    @ManyToOne
    private Estados idEstado;
    @JoinColumn(name = "MARCADOR_JUEGO", referencedColumnName = "ID")
    @ManyToOne
    private Marcador marcadorJuego;

    public VentaApuestas() {
    }

    public VentaApuestas(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getValorApuesta() {
        return valorApuesta;
    }

    public void setValorApuesta(Integer valorApuesta) {
        this.valorApuesta = valorApuesta;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Integer getEstadoDescripcion() {
        return estadoDescripcion;
    }

    public void setEstadoDescripcion(Integer estadoDescripcion) {
        this.estadoDescripcion = estadoDescripcion;
    }

    public Apuesta getIdApuesta() {
        return idApuesta;
    }

    public void setIdApuesta(Apuesta idApuesta) {
        this.idApuesta = idApuesta;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Estados getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estados idEstado) {
        this.idEstado = idEstado;
    }

    public Marcador getMarcadorJuego() {
        return marcadorJuego;
    }

    public void setMarcadorJuego(Marcador marcadorJuego) {
        this.marcadorJuego = marcadorJuego;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaApuestas)) {
            return false;
        }
        VentaApuestas other = (VentaApuestas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniminuto.entity.VentaApuestas[ id=" + id + " ]";
    }
    
}
