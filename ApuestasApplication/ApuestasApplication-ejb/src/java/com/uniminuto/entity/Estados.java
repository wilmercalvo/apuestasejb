/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "estados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estados.findAll", query = "SELECT e FROM Estados e")
    , @NamedQuery(name = "Estados.findById", query = "SELECT e FROM Estados e WHERE e.id = :id")
    , @NamedQuery(name = "Estados.findByDescripcion", query = "SELECT e FROM Estados e WHERE e.descripcion = :descripcion")})
public class Estados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 200)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(mappedBy = "idEstado")
    private Collection<Rifa> rifaCollection;
    @OneToMany(mappedBy = "idEstado")
    private Collection<VentaRifas> ventaRifasCollection;
    @OneToMany(mappedBy = "idEstado")
    private Collection<VentaApuestas> ventaApuestasCollection;
    @OneToMany(mappedBy = "idEstado")
    private Collection<Apuesta> apuestaCollection;
    @OneToMany(mappedBy = "idEstado")
    private Collection<Usuario> usuarioCollection;
    @OneToMany(mappedBy = "idEstado")
    private Collection<Partido> partidoCollection;

    public Estados() {
    }

    public Estados(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<Rifa> getRifaCollection() {
        return rifaCollection;
    }

    public void setRifaCollection(Collection<Rifa> rifaCollection) {
        this.rifaCollection = rifaCollection;
    }

    @XmlTransient
    public Collection<VentaRifas> getVentaRifasCollection() {
        return ventaRifasCollection;
    }

    public void setVentaRifasCollection(Collection<VentaRifas> ventaRifasCollection) {
        this.ventaRifasCollection = ventaRifasCollection;
    }

    @XmlTransient
    public Collection<VentaApuestas> getVentaApuestasCollection() {
        return ventaApuestasCollection;
    }

    public void setVentaApuestasCollection(Collection<VentaApuestas> ventaApuestasCollection) {
        this.ventaApuestasCollection = ventaApuestasCollection;
    }

    @XmlTransient
    public Collection<Apuesta> getApuestaCollection() {
        return apuestaCollection;
    }

    public void setApuestaCollection(Collection<Apuesta> apuestaCollection) {
        this.apuestaCollection = apuestaCollection;
    }

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    @XmlTransient
    public Collection<Partido> getPartidoCollection() {
        return partidoCollection;
    }

    public void setPartidoCollection(Collection<Partido> partidoCollection) {
        this.partidoCollection = partidoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estados)) {
            return false;
        }
        Estados other = (Estados) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniminuto.entity.Estados[ id=" + id + " ]";
    }
    
}
