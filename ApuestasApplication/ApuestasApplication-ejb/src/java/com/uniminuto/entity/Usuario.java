/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findById", query = "SELECT u FROM Usuario u WHERE u.id = :id")
    , @NamedQuery(name = "Usuario.findByNombre", query = "SELECT u FROM Usuario u WHERE u.nombre = :nombre")
    , @NamedQuery(name = "Usuario.findByUsuario", query = "SELECT u FROM Usuario u WHERE u.usuario = :usuario")
    , @NamedQuery(name = "Usuario.findByPassword", query = "SELECT u FROM Usuario u WHERE u.password = :password")
    , @NamedQuery(name = "Usuario.findByNumeroDocumento", query = "SELECT u FROM Usuario u WHERE u.numeroDocumento = :numeroDocumento")
    , @NamedQuery(name = "Usuario.findByCupoDinero", query = "SELECT u FROM Usuario u WHERE u.cupoDinero = :cupoDinero")
    , @NamedQuery(name = "Usuario.findByEstadoDescripcion", query = "SELECT u FROM Usuario u WHERE u.estadoDescripcion = :estadoDescripcion")
    , @NamedQuery(name = "Usuario.login", query = "SELECT u FROM Usuario u WHERE u.usuario = :usuario AND u.password = :password")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 200)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 200)
    @Column(name = "USUARIO")
    private String usuario;
    @Size(max = 200)
    @Column(name = "PASSWORD")
    private String password;
    @Size(max = 200)
    @Column(name = "NUMERO_DOCUMENTO")
    private String numeroDocumento;
    @Column(name = "CUPO_DINERO")
    private Integer cupoDinero;
    @Column(name = "ESTADO_DESCRIPCION")
    private Integer estadoDescripcion;
    @OneToMany(mappedBy = "idUsuario")
    private Collection<VentaRifas> ventaRifasCollection;
    @OneToMany(mappedBy = "idUsuario")
    private Collection<VentaApuestas> ventaApuestasCollection;
    @JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID")
    @ManyToOne
    private Estados idEstado;
    @JoinColumn(name = "ID_PERFIL", referencedColumnName = "ID")
    @ManyToOne
    private Perfil idPerfil;

    public Usuario() {
    }

    public Usuario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Integer getCupoDinero() {
        return cupoDinero;
    }

    public void setCupoDinero(Integer cupoDinero) {
        this.cupoDinero = cupoDinero;
    }

    public Integer getEstadoDescripcion() {
        return estadoDescripcion;
    }

    public void setEstadoDescripcion(Integer estadoDescripcion) {
        this.estadoDescripcion = estadoDescripcion;
    }

    @XmlTransient
    public Collection<VentaRifas> getVentaRifasCollection() {
        return ventaRifasCollection;
    }

    public void setVentaRifasCollection(Collection<VentaRifas> ventaRifasCollection) {
        this.ventaRifasCollection = ventaRifasCollection;
    }

    @XmlTransient
    public Collection<VentaApuestas> getVentaApuestasCollection() {
        return ventaApuestasCollection;
    }

    public void setVentaApuestasCollection(Collection<VentaApuestas> ventaApuestasCollection) {
        this.ventaApuestasCollection = ventaApuestasCollection;
    }

    public Estados getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estados idEstado) {
        this.idEstado = idEstado;
    }

    public Perfil getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Perfil idPerfil) {
        this.idPerfil = idPerfil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniminuto.entity.Usuario[ id=" + id + " ]";
    }
    
}
