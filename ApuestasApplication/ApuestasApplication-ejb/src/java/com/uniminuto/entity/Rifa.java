/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "rifa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rifa.findAll", query = "SELECT r FROM Rifa r")
    , @NamedQuery(name = "Rifa.findById", query = "SELECT r FROM Rifa r WHERE r.id = :id")
    , @NamedQuery(name = "Rifa.findByNombre", query = "SELECT r FROM Rifa r WHERE r.nombre = :nombre")
    , @NamedQuery(name = "Rifa.findByDescripcion", query = "SELECT r FROM Rifa r WHERE r.descripcion = :descripcion")
    , @NamedQuery(name = "Rifa.findByNumeroMaxRifa", query = "SELECT r FROM Rifa r WHERE r.numeroMaxRifa = :numeroMaxRifa")
    , @NamedQuery(name = "Rifa.findByValor", query = "SELECT r FROM Rifa r WHERE r.valor = :valor")
    , @NamedQuery(name = "Rifa.findByResultado", query = "SELECT r FROM Rifa r WHERE r.resultado = :resultado")
    , @NamedQuery(name = "Rifa.findByFechaRifa", query = "SELECT r FROM Rifa r WHERE r.fechaRifa = :fechaRifa")
    , @NamedQuery(name = "Rifa.findByEstadoDescripcion", query = "SELECT r FROM Rifa r WHERE r.estadoDescripcion = :estadoDescripcion")
    , @NamedQuery(name = "Rifa.findByCuposActualParticipantes", query = "SELECT r FROM Rifa r WHERE r.cuposActualParticipantes = :cuposActualParticipantes")
    , @NamedQuery(name = "Rifa.findByCuposParticipantes", query = "SELECT r FROM Rifa r WHERE r.cuposParticipantes = :cuposParticipantes")})
public class Rifa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 200)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 200)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "NUMERO_MAX_RIFA")
    private Integer numeroMaxRifa;
    @Column(name = "VALOR")
    private Integer valor;
    @Size(max = 200)
    @Column(name = "RESULTADO")
    private String resultado;
    @Size(max = 200)
    @Column(name = "FECHA_RIFA")
    private String fechaRifa;
    @Column(name = "ESTADO_DESCRIPCION")
    private Integer estadoDescripcion;
    @Column(name = "CUPOS_ACTUAL_PARTICIPANTES")
    private Integer cuposActualParticipantes;
    @Column(name = "CUPOS_PARTICIPANTES")
    private Integer cuposParticipantes;
    @JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID")
    @ManyToOne
    private Estados idEstado;
    @JoinColumn(name = "PREMIO_PRINCIPAL", referencedColumnName = "ID")
    @ManyToOne
    private Premios premioPrincipal;
    @JoinColumn(name = "ID_PREMIO_SECUNDARIO_1", referencedColumnName = "ID")
    @ManyToOne
    private Premios idPremioSecundario1;
    @JoinColumn(name = "ID_PREMIO_SECUNDARIO_2", referencedColumnName = "ID")
    @ManyToOne
    private Premios idPremioSecundario2;
    @JoinColumn(name = "ID_PREMIO_SECUNDARIO_3", referencedColumnName = "ID")
    @ManyToOne
    private Premios idPremioSecundario3;
    @JoinColumn(name = "ID_PREMIO_SECUNDARIO_4", referencedColumnName = "ID")
    @ManyToOne
    private Premios idPremioSecundario4;
    @OneToMany(mappedBy = "idRifa")
    private Collection<VentaRifas> ventaRifasCollection;

    public Rifa() {
    }

    public Rifa(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getNumeroMaxRifa() {
        return numeroMaxRifa;
    }

    public void setNumeroMaxRifa(Integer numeroMaxRifa) {
        this.numeroMaxRifa = numeroMaxRifa;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getFechaRifa() {
        return fechaRifa;
    }

    public void setFechaRifa(String fechaRifa) {
        this.fechaRifa = fechaRifa;
    }

    public Integer getEstadoDescripcion() {
        return estadoDescripcion;
    }

    public void setEstadoDescripcion(Integer estadoDescripcion) {
        this.estadoDescripcion = estadoDescripcion;
    }

    public Integer getCuposActualParticipantes() {
        return cuposActualParticipantes;
    }

    public void setCuposActualParticipantes(Integer cuposActualParticipantes) {
        this.cuposActualParticipantes = cuposActualParticipantes;
    }

    public Integer getCuposParticipantes() {
        return cuposParticipantes;
    }

    public void setCuposParticipantes(Integer cuposParticipantes) {
        this.cuposParticipantes = cuposParticipantes;
    }

    public Estados getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estados idEstado) {
        this.idEstado = idEstado;
    }

    public Premios getPremioPrincipal() {
        return premioPrincipal;
    }

    public void setPremioPrincipal(Premios premioPrincipal) {
        this.premioPrincipal = premioPrincipal;
    }

    public Premios getIdPremioSecundario1() {
        return idPremioSecundario1;
    }

    public void setIdPremioSecundario1(Premios idPremioSecundario1) {
        this.idPremioSecundario1 = idPremioSecundario1;
    }

    public Premios getIdPremioSecundario2() {
        return idPremioSecundario2;
    }

    public void setIdPremioSecundario2(Premios idPremioSecundario2) {
        this.idPremioSecundario2 = idPremioSecundario2;
    }

    public Premios getIdPremioSecundario3() {
        return idPremioSecundario3;
    }

    public void setIdPremioSecundario3(Premios idPremioSecundario3) {
        this.idPremioSecundario3 = idPremioSecundario3;
    }

    public Premios getIdPremioSecundario4() {
        return idPremioSecundario4;
    }

    public void setIdPremioSecundario4(Premios idPremioSecundario4) {
        this.idPremioSecundario4 = idPremioSecundario4;
    }

    @XmlTransient
    public Collection<VentaRifas> getVentaRifasCollection() {
        return ventaRifasCollection;
    }

    public void setVentaRifasCollection(Collection<VentaRifas> ventaRifasCollection) {
        this.ventaRifasCollection = ventaRifasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rifa)) {
            return false;
        }
        Rifa other = (Rifa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniminuto.entity.Rifa[ id=" + id + " ]";
    }
    
}
