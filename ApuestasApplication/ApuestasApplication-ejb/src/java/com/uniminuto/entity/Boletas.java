/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@Entity
@Table(name = "boletas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Boletas.findAll", query = "SELECT b FROM Boletas b")
    , @NamedQuery(name = "Boletas.findById", query = "SELECT b FROM Boletas b WHERE b.id = :id")
    , @NamedQuery(name = "Boletas.findByNombre", query = "SELECT b FROM Boletas b WHERE b.nombre = :nombre")
    , @NamedQuery(name = "Boletas.findByDescripcion", query = "SELECT b FROM Boletas b WHERE b.descripcion = :descripcion")
    , @NamedQuery(name = "Boletas.findByIdEstado", query = "SELECT b FROM Boletas b WHERE b.idEstado = :idEstado")
    , @NamedQuery(name = "Boletas.findByEstadoDescripcion", query = "SELECT b FROM Boletas b WHERE b.estadoDescripcion = :estadoDescripcion")})
public class Boletas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 200)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 200)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "ID_ESTADO")
    private Integer idEstado;
    @Column(name = "ESTADO_DESCRIPCION")
    private Integer estadoDescripcion;

    public Boletas() {
    }

    public Boletas(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getEstadoDescripcion() {
        return estadoDescripcion;
    }

    public void setEstadoDescripcion(Integer estadoDescripcion) {
        this.estadoDescripcion = estadoDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Boletas)) {
            return false;
        }
        Boletas other = (Boletas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniminuto.entity.Boletas[ id=" + id + " ]";
    }
    
}
