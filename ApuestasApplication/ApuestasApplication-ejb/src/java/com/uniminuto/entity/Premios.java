/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "premios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Premios.findAll", query = "SELECT p FROM Premios p")
    , @NamedQuery(name = "Premios.findById", query = "SELECT p FROM Premios p WHERE p.id = :id")
    , @NamedQuery(name = "Premios.findByDescripcion", query = "SELECT p FROM Premios p WHERE p.descripcion = :descripcion")})
public class Premios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 200)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(mappedBy = "premioPrincipal")
    private Collection<Rifa> rifaCollection;
    @OneToMany(mappedBy = "idPremioSecundario1")
    private Collection<Rifa> rifaCollection1;
    @OneToMany(mappedBy = "idPremioSecundario2")
    private Collection<Rifa> rifaCollection2;
    @OneToMany(mappedBy = "idPremioSecundario3")
    private Collection<Rifa> rifaCollection3;
    @OneToMany(mappedBy = "idPremioSecundario4")
    private Collection<Rifa> rifaCollection4;

    public Premios() {
    }

    public Premios(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<Rifa> getRifaCollection() {
        return rifaCollection;
    }

    public void setRifaCollection(Collection<Rifa> rifaCollection) {
        this.rifaCollection = rifaCollection;
    }

    @XmlTransient
    public Collection<Rifa> getRifaCollection1() {
        return rifaCollection1;
    }

    public void setRifaCollection1(Collection<Rifa> rifaCollection1) {
        this.rifaCollection1 = rifaCollection1;
    }

    @XmlTransient
    public Collection<Rifa> getRifaCollection2() {
        return rifaCollection2;
    }

    public void setRifaCollection2(Collection<Rifa> rifaCollection2) {
        this.rifaCollection2 = rifaCollection2;
    }

    @XmlTransient
    public Collection<Rifa> getRifaCollection3() {
        return rifaCollection3;
    }

    public void setRifaCollection3(Collection<Rifa> rifaCollection3) {
        this.rifaCollection3 = rifaCollection3;
    }

    @XmlTransient
    public Collection<Rifa> getRifaCollection4() {
        return rifaCollection4;
    }

    public void setRifaCollection4(Collection<Rifa> rifaCollection4) {
        this.rifaCollection4 = rifaCollection4;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Premios)) {
            return false;
        }
        Premios other = (Premios) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniminuto.entity.Premios[ id=" + id + " ]";
    }
    
}
