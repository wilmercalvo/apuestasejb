/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@Entity
@Table(name = "venta_rifas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VentaRifas.findAll", query = "SELECT v FROM VentaRifas v")
    , @NamedQuery(name = "VentaRifas.findById", query = "SELECT v FROM VentaRifas v WHERE v.id = :id")
    , @NamedQuery(name = "VentaRifas.findByValorRifa", query = "SELECT v FROM VentaRifas v WHERE v.valorRifa = :valorRifa")
    , @NamedQuery(name = "VentaRifas.findByResultado", query = "SELECT v FROM VentaRifas v WHERE v.resultado = :resultado")
    , @NamedQuery(name = "VentaRifas.findByTipoPremio", query = "SELECT v FROM VentaRifas v WHERE v.tipoPremio = :tipoPremio")
    , @NamedQuery(name = "VentaRifas.findByMarcadorJuego", query = "SELECT v FROM VentaRifas v WHERE v.marcadorJuego = :marcadorJuego")
    , @NamedQuery(name = "VentaRifas.findByEstadoDescripcion", query = "SELECT v FROM VentaRifas v WHERE v.estadoDescripcion = :estadoDescripcion")})
public class VentaRifas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "VALOR_RIFA")
    private Integer valorRifa;
    @Size(max = 200)
    @Column(name = "RESULTADO")
    private String resultado;
    @Size(max = 200)
    @Column(name = "TIPO_PREMIO")
    private String tipoPremio;
    @Column(name = "MARCADOR_JUEGO")
    private Integer marcadorJuego;
    @Column(name = "ESTADO_DESCRIPCION")
    private Integer estadoDescripcion;
    @JoinColumn(name = "ID_RIFA", referencedColumnName = "ID")
    @ManyToOne
    private Rifa idRifa;
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID")
    @ManyToOne
    private Estados idEstado;

    public VentaRifas() {
    }

    public VentaRifas(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getValorRifa() {
        return valorRifa;
    }

    public void setValorRifa(Integer valorRifa) {
        this.valorRifa = valorRifa;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getTipoPremio() {
        return tipoPremio;
    }

    public void setTipoPremio(String tipoPremio) {
        this.tipoPremio = tipoPremio;
    }

    public Integer getMarcadorJuego() {
        return marcadorJuego;
    }

    public void setMarcadorJuego(Integer marcadorJuego) {
        this.marcadorJuego = marcadorJuego;
    }

    public Integer getEstadoDescripcion() {
        return estadoDescripcion;
    }

    public void setEstadoDescripcion(Integer estadoDescripcion) {
        this.estadoDescripcion = estadoDescripcion;
    }

    public Rifa getIdRifa() {
        return idRifa;
    }

    public void setIdRifa(Rifa idRifa) {
        this.idRifa = idRifa;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Estados getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estados idEstado) {
        this.idEstado = idEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaRifas)) {
            return false;
        }
        VentaRifas other = (VentaRifas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniminuto.entity.VentaRifas[ id=" + id + " ]";
    }
    
}
