/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniminuto.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@Table(name = "partido")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Partido.findAll", query = "SELECT p FROM Partido p")
    , @NamedQuery(name = "Partido.findById", query = "SELECT p FROM Partido p WHERE p.id = :id")
    , @NamedQuery(name = "Partido.findByNombre", query = "SELECT p FROM Partido p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Partido.findByMarcadorJuego", query = "SELECT p FROM Partido p WHERE p.marcadorJuego = :marcadorJuego")
    , @NamedQuery(name = "Partido.findByFechaPartido", query = "SELECT p FROM Partido p WHERE p.fechaPartido = :fechaPartido")
    , @NamedQuery(name = "Partido.findByHoraPartido", query = "SELECT p FROM Partido p WHERE p.horaPartido = :horaPartido")})
public class Partido implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 200)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "MARCADOR_JUEGO")
    private Integer marcadorJuego;
    @Size(max = 200)
    @Column(name = "FECHA_PARTIDO")
    private String fechaPartido;
    @Size(max = 200)
    @Column(name = "HORA_PARTIDO")
    private String horaPartido;
    @OneToMany(mappedBy = "idPartido")
    private Collection<Apuesta> apuestaCollection;
    @JoinColumn(name = "ID_DEPORTE", referencedColumnName = "ID")
    @ManyToOne
    private Deporte idDeporte;
    @JoinColumn(name = "ID_EQUIPO_LOCAL", referencedColumnName = "ID")
    @ManyToOne
    private EquipoJugador idEquipoLocal;
    @JoinColumn(name = "ID_EQUIPO_VISITANTE", referencedColumnName = "ID")
    @ManyToOne
    private EquipoJugador idEquipoVisitante;
    @JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID")
    @ManyToOne
    private Estados idEstado;

    public Partido() {
    }

    public Partido(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getMarcadorJuego() {
        return marcadorJuego;
    }

    public void setMarcadorJuego(Integer marcadorJuego) {
        this.marcadorJuego = marcadorJuego;
    }

    public String getFechaPartido() {
        return fechaPartido;
    }

    public void setFechaPartido(String fechaPartido) {
        this.fechaPartido = fechaPartido;
    }

    public String getHoraPartido() {
        return horaPartido;
    }

    public void setHoraPartido(String horaPartido) {
        this.horaPartido = horaPartido;
    }

    @XmlTransient
    public Collection<Apuesta> getApuestaCollection() {
        return apuestaCollection;
    }

    public void setApuestaCollection(Collection<Apuesta> apuestaCollection) {
        this.apuestaCollection = apuestaCollection;
    }

    public Deporte getIdDeporte() {
        return idDeporte;
    }

    public void setIdDeporte(Deporte idDeporte) {
        this.idDeporte = idDeporte;
    }

    public EquipoJugador getIdEquipoLocal() {
        return idEquipoLocal;
    }

    public void setIdEquipoLocal(EquipoJugador idEquipoLocal) {
        this.idEquipoLocal = idEquipoLocal;
    }

    public EquipoJugador getIdEquipoVisitante() {
        return idEquipoVisitante;
    }

    public void setIdEquipoVisitante(EquipoJugador idEquipoVisitante) {
        this.idEquipoVisitante = idEquipoVisitante;
    }

    public Estados getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estados idEstado) {
        this.idEstado = idEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partido)) {
            return false;
        }
        Partido other = (Partido) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniminuto.entity.Partido[ id=" + id + " ]";
    }
    
}
