package com.uniminuto.entity;

import com.uniminuto.entity.Apuesta;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.Rifa;
import com.uniminuto.entity.Usuario;
import com.uniminuto.entity.VentaApuestas;
import com.uniminuto.entity.VentaRifas;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Estados.class)
public class Estados_ { 

    public static volatile SingularAttribute<Estados, String> descripcion;
    public static volatile CollectionAttribute<Estados, Partido> partidoCollection;
    public static volatile CollectionAttribute<Estados, VentaRifas> ventaRifasCollection;
    public static volatile CollectionAttribute<Estados, Rifa> rifaCollection;
    public static volatile SingularAttribute<Estados, Integer> id;
    public static volatile CollectionAttribute<Estados, VentaApuestas> ventaApuestasCollection;
    public static volatile CollectionAttribute<Estados, Usuario> usuarioCollection;
    public static volatile CollectionAttribute<Estados, Apuesta> apuestaCollection;

}