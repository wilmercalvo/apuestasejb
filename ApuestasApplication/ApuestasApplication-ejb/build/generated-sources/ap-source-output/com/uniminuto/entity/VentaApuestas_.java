package com.uniminuto.entity;

import com.uniminuto.entity.Apuesta;
import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Marcador;
import com.uniminuto.entity.Usuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(VentaApuestas.class)
public class VentaApuestas_ { 

    public static volatile SingularAttribute<VentaApuestas, Integer> valorApuesta;
    public static volatile SingularAttribute<VentaApuestas, Estados> idEstado;
    public static volatile SingularAttribute<VentaApuestas, String> resultado;
    public static volatile SingularAttribute<VentaApuestas, Marcador> marcadorJuego;
    public static volatile SingularAttribute<VentaApuestas, Usuario> idUsuario;
    public static volatile SingularAttribute<VentaApuestas, Integer> estadoDescripcion;
    public static volatile SingularAttribute<VentaApuestas, Integer> id;
    public static volatile SingularAttribute<VentaApuestas, Apuesta> idApuesta;

}