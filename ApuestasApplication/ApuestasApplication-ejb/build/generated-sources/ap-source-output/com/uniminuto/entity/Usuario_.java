package com.uniminuto.entity;

import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Perfil;
import com.uniminuto.entity.VentaApuestas;
import com.uniminuto.entity.VentaRifas;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, String> password;
    public static volatile SingularAttribute<Usuario, Estados> idEstado;
    public static volatile SingularAttribute<Usuario, Integer> cupoDinero;
    public static volatile CollectionAttribute<Usuario, VentaRifas> ventaRifasCollection;
    public static volatile SingularAttribute<Usuario, Perfil> idPerfil;
    public static volatile SingularAttribute<Usuario, String> usuario;
    public static volatile SingularAttribute<Usuario, Integer> estadoDescripcion;
    public static volatile SingularAttribute<Usuario, Integer> id;
    public static volatile SingularAttribute<Usuario, String> numeroDocumento;
    public static volatile CollectionAttribute<Usuario, VentaApuestas> ventaApuestasCollection;
    public static volatile SingularAttribute<Usuario, String> nombre;

}