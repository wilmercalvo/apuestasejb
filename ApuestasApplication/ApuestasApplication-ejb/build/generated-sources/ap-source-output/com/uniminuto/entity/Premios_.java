package com.uniminuto.entity;

import com.uniminuto.entity.Rifa;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Premios.class)
public class Premios_ { 

    public static volatile SingularAttribute<Premios, String> descripcion;
    public static volatile CollectionAttribute<Premios, Rifa> rifaCollection;
    public static volatile CollectionAttribute<Premios, Rifa> rifaCollection3;
    public static volatile CollectionAttribute<Premios, Rifa> rifaCollection2;
    public static volatile CollectionAttribute<Premios, Rifa> rifaCollection1;
    public static volatile SingularAttribute<Premios, Integer> id;
    public static volatile CollectionAttribute<Premios, Rifa> rifaCollection4;

}