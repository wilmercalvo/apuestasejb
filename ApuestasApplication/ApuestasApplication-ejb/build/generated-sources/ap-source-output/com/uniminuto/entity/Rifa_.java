package com.uniminuto.entity;

import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Premios;
import com.uniminuto.entity.VentaRifas;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Rifa.class)
public class Rifa_ { 

    public static volatile SingularAttribute<Rifa, String> descripcion;
    public static volatile SingularAttribute<Rifa, Integer> numeroMaxRifa;
    public static volatile SingularAttribute<Rifa, String> resultado;
    public static volatile SingularAttribute<Rifa, Integer> valor;
    public static volatile SingularAttribute<Rifa, Integer> estadoDescripcion;
    public static volatile SingularAttribute<Rifa, String> nombre;
    public static volatile SingularAttribute<Rifa, Estados> idEstado;
    public static volatile SingularAttribute<Rifa, Premios> premioPrincipal;
    public static volatile SingularAttribute<Rifa, Premios> idPremioSecundario4;
    public static volatile CollectionAttribute<Rifa, VentaRifas> ventaRifasCollection;
    public static volatile SingularAttribute<Rifa, String> fechaRifa;
    public static volatile SingularAttribute<Rifa, Integer> id;
    public static volatile SingularAttribute<Rifa, Premios> idPremioSecundario3;
    public static volatile SingularAttribute<Rifa, Premios> idPremioSecundario2;
    public static volatile SingularAttribute<Rifa, Integer> cuposActualParticipantes;
    public static volatile SingularAttribute<Rifa, Premios> idPremioSecundario1;
    public static volatile SingularAttribute<Rifa, Integer> cuposParticipantes;

}