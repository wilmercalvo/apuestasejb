package com.uniminuto.entity;

import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Partido;
import com.uniminuto.entity.VentaApuestas;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Apuesta.class)
public class Apuesta_ { 

    public static volatile SingularAttribute<Apuesta, String> descripcion;
    public static volatile SingularAttribute<Apuesta, Estados> idEstado;
    public static volatile SingularAttribute<Apuesta, Integer> valor;
    public static volatile SingularAttribute<Apuesta, Integer> estadoDescripcion;
    public static volatile SingularAttribute<Apuesta, Partido> idPartido;
    public static volatile SingularAttribute<Apuesta, Integer> id;
    public static volatile CollectionAttribute<Apuesta, VentaApuestas> ventaApuestasCollection;
    public static volatile SingularAttribute<Apuesta, String> nombre;

}