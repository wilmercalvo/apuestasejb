package com.uniminuto.entity;

import com.uniminuto.entity.Apuesta;
import com.uniminuto.entity.Deporte;
import com.uniminuto.entity.EquipoJugador;
import com.uniminuto.entity.Estados;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Partido.class)
public class Partido_ { 

    public static volatile SingularAttribute<Partido, Estados> idEstado;
    public static volatile SingularAttribute<Partido, Integer> marcadorJuego;
    public static volatile SingularAttribute<Partido, EquipoJugador> idEquipoVisitante;
    public static volatile SingularAttribute<Partido, String> fechaPartido;
    public static volatile SingularAttribute<Partido, String> horaPartido;
    public static volatile SingularAttribute<Partido, Integer> id;
    public static volatile SingularAttribute<Partido, EquipoJugador> idEquipoLocal;
    public static volatile SingularAttribute<Partido, String> nombre;
    public static volatile SingularAttribute<Partido, Deporte> idDeporte;
    public static volatile CollectionAttribute<Partido, Apuesta> apuestaCollection;

}