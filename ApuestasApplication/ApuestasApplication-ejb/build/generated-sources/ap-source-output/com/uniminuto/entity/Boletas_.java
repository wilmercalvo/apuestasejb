package com.uniminuto.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Boletas.class)
public class Boletas_ { 

    public static volatile SingularAttribute<Boletas, String> descripcion;
    public static volatile SingularAttribute<Boletas, Integer> idEstado;
    public static volatile SingularAttribute<Boletas, Integer> estadoDescripcion;
    public static volatile SingularAttribute<Boletas, Integer> id;
    public static volatile SingularAttribute<Boletas, String> nombre;

}