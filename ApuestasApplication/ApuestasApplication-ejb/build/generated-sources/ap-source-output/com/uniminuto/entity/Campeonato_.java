package com.uniminuto.entity;

import com.uniminuto.entity.EquipoJugador;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Campeonato.class)
public class Campeonato_ { 

    public static volatile SingularAttribute<Campeonato, String> descripcion;
    public static volatile CollectionAttribute<Campeonato, EquipoJugador> equipoJugadorCollection;
    public static volatile SingularAttribute<Campeonato, Integer> id;

}