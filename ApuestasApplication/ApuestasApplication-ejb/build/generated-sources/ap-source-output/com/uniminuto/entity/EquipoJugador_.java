package com.uniminuto.entity;

import com.uniminuto.entity.Campeonato;
import com.uniminuto.entity.Partido;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(EquipoJugador.class)
public class EquipoJugador_ { 

    public static volatile CollectionAttribute<EquipoJugador, Partido> partidoCollection;
    public static volatile CollectionAttribute<EquipoJugador, Partido> partidoCollection1;
    public static volatile SingularAttribute<EquipoJugador, Campeonato> idCampeonato;
    public static volatile SingularAttribute<EquipoJugador, Integer> id;
    public static volatile SingularAttribute<EquipoJugador, String> nombre;

}