package com.uniminuto.entity;

import com.uniminuto.entity.Estados;
import com.uniminuto.entity.Rifa;
import com.uniminuto.entity.Usuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(VentaRifas.class)
public class VentaRifas_ { 

    public static volatile SingularAttribute<VentaRifas, Estados> idEstado;
    public static volatile SingularAttribute<VentaRifas, Integer> valorRifa;
    public static volatile SingularAttribute<VentaRifas, String> resultado;
    public static volatile SingularAttribute<VentaRifas, Integer> marcadorJuego;
    public static volatile SingularAttribute<VentaRifas, Usuario> idUsuario;
    public static volatile SingularAttribute<VentaRifas, Rifa> idRifa;
    public static volatile SingularAttribute<VentaRifas, Integer> estadoDescripcion;
    public static volatile SingularAttribute<VentaRifas, Integer> id;
    public static volatile SingularAttribute<VentaRifas, String> tipoPremio;

}