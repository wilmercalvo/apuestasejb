package com.uniminuto.entity;

import com.uniminuto.entity.Usuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Perfil.class)
public class Perfil_ { 

    public static volatile SingularAttribute<Perfil, String> descripcion;
    public static volatile SingularAttribute<Perfil, Integer> id;
    public static volatile CollectionAttribute<Perfil, Usuario> usuarioCollection;

}