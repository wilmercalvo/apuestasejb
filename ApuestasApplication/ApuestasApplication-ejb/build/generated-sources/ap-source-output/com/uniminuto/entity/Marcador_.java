package com.uniminuto.entity;

import com.uniminuto.entity.VentaApuestas;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Marcador.class)
public class Marcador_ { 

    public static volatile SingularAttribute<Marcador, String> descripcion;
    public static volatile SingularAttribute<Marcador, String> valor;
    public static volatile SingularAttribute<Marcador, Integer> id;
    public static volatile CollectionAttribute<Marcador, VentaApuestas> ventaApuestasCollection;

}