package com.uniminuto.entity;

import com.uniminuto.entity.Partido;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-22T23:46:30")
@StaticMetamodel(Deporte.class)
public class Deporte_ { 

    public static volatile SingularAttribute<Deporte, String> descripcion;
    public static volatile CollectionAttribute<Deporte, Partido> partidoCollection;
    public static volatile SingularAttribute<Deporte, Integer> id;

}