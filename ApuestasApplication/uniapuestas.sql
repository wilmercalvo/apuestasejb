-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-03-2019 a las 05:30:08
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `uniapuestas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apuesta`
--

CREATE TABLE `apuesta` (
  `ID` int(9) NOT NULL,
  `NOMBRE` varchar(200) DEFAULT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL,
  `ID_PARTIDO` int(9) DEFAULT NULL,
  `VALOR` int(9) DEFAULT NULL,
  `ID_ESTADO` int(9) DEFAULT NULL,
  `ESTADO_DESCRIPCION` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boletas`
--

CREATE TABLE `boletas` (
  `ID` int(9) NOT NULL,
  `NOMBRE` varchar(200) DEFAULT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL,
  `ID_ESTADO` int(9) DEFAULT NULL,
  `ESTADO_DESCRIPCION` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campeonato`
--

CREATE TABLE `campeonato` (
  `ID` int(9) NOT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deporte`
--

CREATE TABLE `deporte` (
  `ID` int(9) NOT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo_jugador`
--

CREATE TABLE `equipo_jugador` (
  `ID` int(9) NOT NULL,
  `NOMBRE` varchar(200) DEFAULT NULL,
  `ID_CAMPEONATO` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `ID` int(9) NOT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcador`
--

CREATE TABLE `marcador` (
  `ID` int(9) NOT NULL,
  `VALOR` varchar(200) DEFAULT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametros`
--

CREATE TABLE `parametros` (
  `ID` int(9) NOT NULL,
  `NOMBRE` varchar(200) DEFAULT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partido`
--

CREATE TABLE `partido` (
  `ID` int(9) NOT NULL,
  `NOMBRE` varchar(200) DEFAULT NULL,
  `ID_DEPORTE` int(9) DEFAULT NULL,
  `ID_EQUIPO_LOCAL` int(9) DEFAULT NULL,
  `ID_EQUIPO_VISITANTE` int(9) DEFAULT NULL,
  `MARCADOR_JUEGO` int(9) DEFAULT NULL,
  `FECHA_PARTIDO` varchar(200) DEFAULT NULL,
  `HORA_PARTIDO` varchar(200) DEFAULT NULL,
  `ID_ESTADO` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `ID` int(9) NOT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `premios`
--

CREATE TABLE `premios` (
  `ID` int(9) NOT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rifa`
--

CREATE TABLE `rifa` (
  `ID` int(9) NOT NULL,
  `NOMBRE` varchar(200) DEFAULT NULL,
  `DESCRIPCION` varchar(200) DEFAULT NULL,
  `NUMERO_MAX_RIFA` int(9) DEFAULT NULL,
  `VALOR` int(9) DEFAULT NULL,
  `RESULTADO` varchar(200) DEFAULT NULL,
  `FECHA_RIFA` varchar(200) DEFAULT NULL,
  `PREMIO_PRINCIPAL` int(9) DEFAULT NULL,
  `ID_PREMIO_SECUNDARIO_1` int(9) DEFAULT NULL,
  `ID_PREMIO_SECUNDARIO_2` int(9) DEFAULT NULL,
  `ID_PREMIO_SECUNDARIO_3` int(9) DEFAULT NULL,
  `ID_PREMIO_SECUNDARIO_4` int(9) DEFAULT NULL,
  `ID_ESTADO` int(9) DEFAULT NULL,
  `ESTADO_DESCRIPCION` int(9) DEFAULT NULL,
  `CUPOS_ACTUAL_PARTICIPANTES` int(9) DEFAULT NULL,
  `CUPOS_PARTICIPANTES` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `ID` int(9) NOT NULL,
  `NOMBRE` varchar(200) DEFAULT NULL,
  `USUARIO` varchar(200) DEFAULT NULL,
  `PASSWORD` varchar(200) DEFAULT NULL,
  `ID_PERFIL` int(9) DEFAULT NULL,
  `NUMERO_DOCUMENTO` varchar(200) DEFAULT NULL,
  `CUPO_DINERO` int(9) DEFAULT NULL,
  `ID_ESTADO` int(9) DEFAULT NULL,
  `ESTADO_DESCRIPCION` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_apuestas`
--

CREATE TABLE `venta_apuestas` (
  `ID` int(9) NOT NULL,
  `ID_APUESTA` int(9) DEFAULT NULL,
  `ID_USUARIO` int(9) DEFAULT NULL,
  `VALOR_APUESTA` int(9) DEFAULT NULL,
  `RESULTADO` varchar(200) DEFAULT NULL,
  `MARCADOR_JUEGO` int(9) DEFAULT NULL,
  `ID_ESTADO` int(9) DEFAULT NULL,
  `ESTADO_DESCRIPCION` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_rifas`
--

CREATE TABLE `venta_rifas` (
  `ID` int(9) NOT NULL,
  `ID_RIFA` int(9) DEFAULT NULL,
  `ID_USUARIO` int(9) DEFAULT NULL,
  `VALOR_RIFA` int(9) DEFAULT NULL,
  `RESULTADO` varchar(200) DEFAULT NULL,
  `TIPO_PREMIO` varchar(200) DEFAULT NULL,
  `MARCADOR_JUEGO` int(9) DEFAULT NULL,
  `ID_ESTADO` int(9) DEFAULT NULL,
  `ESTADO_DESCRIPCION` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `apuesta`
--
ALTER TABLE `apuesta`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_PARTIDO` (`ID_PARTIDO`),
  ADD KEY `ID_ESTADO` (`ID_ESTADO`);

--
-- Indices de la tabla `boletas`
--
ALTER TABLE `boletas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `campeonato`
--
ALTER TABLE `campeonato`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `deporte`
--
ALTER TABLE `deporte`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `equipo_jugador`
--
ALTER TABLE `equipo_jugador`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CAMPEONATO` (`ID_CAMPEONATO`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `marcador`
--
ALTER TABLE `marcador`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `parametros`
--
ALTER TABLE `parametros`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `partido`
--
ALTER TABLE `partido`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_DEPORTE` (`ID_DEPORTE`),
  ADD KEY `ID_EQUIPO_LOCAL` (`ID_EQUIPO_LOCAL`),
  ADD KEY `ID_EQUIPO_VISITANTE` (`ID_EQUIPO_VISITANTE`),
  ADD KEY `ID_ESTADO` (`ID_ESTADO`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `premios`
--
ALTER TABLE `premios`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `rifa`
--
ALTER TABLE `rifa`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_ESTADO` (`ID_ESTADO`),
  ADD KEY `PREMIO_PRINCIPAL` (`PREMIO_PRINCIPAL`),
  ADD KEY `ID_PREMIO_SECUNDARIO_1` (`ID_PREMIO_SECUNDARIO_1`),
  ADD KEY `ID_PREMIO_SECUNDARIO_2` (`ID_PREMIO_SECUNDARIO_2`),
  ADD KEY `ID_PREMIO_SECUNDARIO_3` (`ID_PREMIO_SECUNDARIO_3`),
  ADD KEY `ID_PREMIO_SECUNDARIO_4` (`ID_PREMIO_SECUNDARIO_4`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_ESTADO` (`ID_ESTADO`),
  ADD KEY `ID_PERFIL` (`ID_PERFIL`);

--
-- Indices de la tabla `venta_apuestas`
--
ALTER TABLE `venta_apuestas`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_APUESTA` (`ID_APUESTA`),
  ADD KEY `ID_USUARIO` (`ID_USUARIO`),
  ADD KEY `ID_ESTADO` (`ID_ESTADO`),
  ADD KEY `MARCADOR_JUEGO` (`MARCADOR_JUEGO`);

--
-- Indices de la tabla `venta_rifas`
--
ALTER TABLE `venta_rifas`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_RIFA` (`ID_RIFA`),
  ADD KEY `ID_USUARIO` (`ID_USUARIO`),
  ADD KEY `ID_ESTADO` (`ID_ESTADO`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `apuesta`
--
ALTER TABLE `apuesta`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `boletas`
--
ALTER TABLE `boletas`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `campeonato`
--
ALTER TABLE `campeonato`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `deporte`
--
ALTER TABLE `deporte`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `equipo_jugador`
--
ALTER TABLE `equipo_jugador`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `marcador`
--
ALTER TABLE `marcador`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `parametros`
--
ALTER TABLE `parametros`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `partido`
--
ALTER TABLE `partido`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `premios`
--
ALTER TABLE `premios`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rifa`
--
ALTER TABLE `rifa`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `venta_apuestas`
--
ALTER TABLE `venta_apuestas`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `venta_rifas`
--
ALTER TABLE `venta_rifas`
  MODIFY `ID` int(9) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `apuesta`
--
ALTER TABLE `apuesta`
  ADD CONSTRAINT `apuesta_ibfk_1` FOREIGN KEY (`ID_PARTIDO`) REFERENCES `partido` (`ID`),
  ADD CONSTRAINT `apuesta_ibfk_2` FOREIGN KEY (`ID_ESTADO`) REFERENCES `estados` (`ID`);

--
-- Filtros para la tabla `equipo_jugador`
--
ALTER TABLE `equipo_jugador`
  ADD CONSTRAINT `equipo_jugador_ibfk_1` FOREIGN KEY (`ID_CAMPEONATO`) REFERENCES `campeonato` (`ID`);

--
-- Filtros para la tabla `partido`
--
ALTER TABLE `partido`
  ADD CONSTRAINT `partido_ibfk_1` FOREIGN KEY (`ID_DEPORTE`) REFERENCES `deporte` (`ID`),
  ADD CONSTRAINT `partido_ibfk_2` FOREIGN KEY (`ID_EQUIPO_LOCAL`) REFERENCES `equipo_jugador` (`ID`),
  ADD CONSTRAINT `partido_ibfk_3` FOREIGN KEY (`ID_EQUIPO_VISITANTE`) REFERENCES `equipo_jugador` (`ID`),
  ADD CONSTRAINT `partido_ibfk_4` FOREIGN KEY (`ID_ESTADO`) REFERENCES `estados` (`ID`);

--
-- Filtros para la tabla `rifa`
--
ALTER TABLE `rifa`
  ADD CONSTRAINT `rifa_ibfk_1` FOREIGN KEY (`ID_ESTADO`) REFERENCES `estados` (`ID`),
  ADD CONSTRAINT `rifa_ibfk_2` FOREIGN KEY (`PREMIO_PRINCIPAL`) REFERENCES `premios` (`ID`),
  ADD CONSTRAINT `rifa_ibfk_3` FOREIGN KEY (`ID_PREMIO_SECUNDARIO_1`) REFERENCES `premios` (`ID`),
  ADD CONSTRAINT `rifa_ibfk_4` FOREIGN KEY (`ID_PREMIO_SECUNDARIO_2`) REFERENCES `premios` (`ID`),
  ADD CONSTRAINT `rifa_ibfk_5` FOREIGN KEY (`ID_PREMIO_SECUNDARIO_3`) REFERENCES `premios` (`ID`),
  ADD CONSTRAINT `rifa_ibfk_6` FOREIGN KEY (`ID_PREMIO_SECUNDARIO_4`) REFERENCES `premios` (`ID`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`ID_ESTADO`) REFERENCES `estados` (`ID`),
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`ID_PERFIL`) REFERENCES `perfil` (`ID`);

--
-- Filtros para la tabla `venta_apuestas`
--
ALTER TABLE `venta_apuestas`
  ADD CONSTRAINT `venta_apuestas_ibfk_1` FOREIGN KEY (`ID_APUESTA`) REFERENCES `apuesta` (`ID`),
  ADD CONSTRAINT `venta_apuestas_ibfk_2` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID`),
  ADD CONSTRAINT `venta_apuestas_ibfk_3` FOREIGN KEY (`ID_ESTADO`) REFERENCES `estados` (`ID`),
  ADD CONSTRAINT `venta_apuestas_ibfk_4` FOREIGN KEY (`MARCADOR_JUEGO`) REFERENCES `marcador` (`ID`);

--
-- Filtros para la tabla `venta_rifas`
--
ALTER TABLE `venta_rifas`
  ADD CONSTRAINT `venta_rifas_ibfk_1` FOREIGN KEY (`ID_RIFA`) REFERENCES `rifa` (`ID`),
  ADD CONSTRAINT `venta_rifas_ibfk_2` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID`),
  ADD CONSTRAINT `venta_rifas_ibfk_3` FOREIGN KEY (`ID_ESTADO`) REFERENCES `estados` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
